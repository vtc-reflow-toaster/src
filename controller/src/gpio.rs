#![allow(dead_code)]

use core::marker::PhantomData;

pub enum Mode {
    Input,
    Output,
    Alternate,
    Analog
}

pub enum OutputType {
    PushPull,
    OpenDrain,
}

pub enum OutputSpeed {
    Low,
    Medium,
    High,
    VeryHigh,
}

pub enum Pull {
    None,
    Up,
    Down
}

macro_rules! setter_call {
    () => {};
    ($name:ident => $setter:ident, $($tail:tt)*) => {
        fn $name(&self, idx: u8) {
            self.$setter(idx, 1);
        }
        #[doc(hidden)]
        fn $setter(&self, idx: u8, bits: u32);
        setter_call!($($tail)*);
    };
    ($name:ident : $type:ty => $setter:ident, $($tail:tt)*) => {
        fn $name(&self, idx: u8, val: $type) {
            self.$setter(idx, u32::from(val));
        }
        #[doc(hidden)]
        fn $setter(&self, idx: u8, bits: u32);
        setter_call!($($tail)*);
    };
    ($name:ident : $type:ty => $setter:ident match { $($pat:pat => $expr:expr),+ }, $($tail:tt)*) => {
        fn $name(&self, idx: u8, val: $type) {
            self.$setter(idx, match val { $($pat => $expr),+ });
        }
        #[doc(hidden)]
        fn $setter(&self, idx: u8, bits: u32);
        setter_call!($($tail)*);
    }
}
macro_rules! setter_impl {
    () => {};
    ($setter:ident => $reg:ident: $width:expr, $($tail:tt)*) => {
        fn $setter(&self, idx: u8, val: u32) {
            unsafe {
                self.$reg.modify(|r, w| w.bits(
                        (r.bits() & !(((1 << $width) - 1) << ($width * idx))) |
                        (val << ($width * idx))));
            }
        }
        setter_impl!($($tail)*);
    };
    ($setter:ident => overwrite $reg:ident: $width:expr, $($tail:tt)*) => {
        fn $setter(&self, idx: u8, val: u32) {
            unsafe {
                self.$reg.write(|w| w.bits(val << ($width * idx)));
            }
        }
        setter_impl!($($tail)*);
    };
}

pub trait GPIORegisterBlock {
    setter_call! {
        mode: Mode => set_mode match {
            Mode::Input => 0b00,
            Mode::Output => 0b01,
            Mode::Alternate => 0b10,
            Mode::Analog => 0b11
        },
        output_type: OutputType => set_output_type match {
            OutputType::PushPull => 0b0,
            OutputType::OpenDrain => 0b1
        },
        output_speed: OutputSpeed => set_output_speed match {
            OutputSpeed::Low => 0b00,
            OutputSpeed::Medium => 0b01,
            OutputSpeed::High => 0b10,
            OutputSpeed::VeryHigh => 0b11
        },
        pull: Pull => set_pull match {
            Pull::None => 0b00,
            Pull::Up => 0b01,
            Pull::Down => 0b10
        },
        write: bool => set_value,
        set => set_output,
        reset => reset_output,
    }
    fn alt_function(&self, idx: u8, val: u32);
    fn read(&self, idx: u8) -> bool;
}

macro_rules! impl_block {
    ($type:ty) => {
        impl GPIORegisterBlock for $type {
            setter_impl! {
                set_mode => moder: 2,
                set_output_type => otyper: 1,
                set_output_speed => ospeedr: 2,
                set_pull => pupdr: 2,
                set_value => odr: 1,
                set_output => overwrite bsrr: 1,
                reset_output => overwrite brr: 1,
            }

            fn alt_function(&self, idx: u8, val: u32) {
                unsafe {
                    if idx < 8 {
                        self.afrl.modify(|r, w| w.bits(
                            (r.bits() & !(0b1111 << (4 * idx))) |
                            (val << (4 * idx))));
                    } else {
                        let idx = idx - 8;
                        self.afrh.modify(|r, w| w.bits(
                            (r.bits() & !(0b1111 << (4 * idx))) |
                            (val << (4 * idx))));
                    }
                }
            }

            fn read(&self, idx: u8) -> bool {
                self.idr.read().bits() & (1 << idx) != 0
            }
        }
    }
}
impl_block!(mcu::gpioa::RegisterBlock);
impl_block!(mcu::gpiob::RegisterBlock);

pub trait GPIOPort {
    const LIMIT: u8;
    type RegisterBlock: GPIORegisterBlock + 'static;
    fn gpio_regs() -> &'static Self::RegisterBlock;
}

macro_rules! ports {
    () => {};
    ($port:path = $type:ty [ $len:expr ], $($tail:tt)*) => {
        impl GPIOPort for $port {
            const LIMIT: u8 = $len;
            type RegisterBlock = $type;
            fn gpio_regs() -> &'static Self::RegisterBlock {
                unsafe {
                    &*<$port>::ptr()
                }
            }
        }
        ports!($($tail)*);
    };
}

ports! {
    mcu::GPIOA = mcu::gpioa::RegisterBlock[16],
    mcu::GPIOB = mcu::gpiob::RegisterBlock[16],
    mcu::GPIOC = mcu::gpiob::RegisterBlock[16],
    mcu::GPIOD = mcu::gpiob::RegisterBlock[0],
    mcu::GPIOE = mcu::gpiob::RegisterBlock[0],
    mcu::GPIOH = mcu::gpiob::RegisterBlock[2],
}

pub struct Builder<P> {
    idx: u8,
    _regs: PhantomData<*const P>,
}
impl<P: GPIOPort> Builder<P> {
    pub fn new(_regs: &P, idx: u8) -> Option<Builder<P>> {
        if idx < P::LIMIT {
            Some(Builder {
                idx,
                _regs: PhantomData{},
            })
        } else {
            None
        }
    }

    pub fn get(self) -> GPIO<P> {
        GPIO {
            idx: self.idx,
            _regs: PhantomData{},
        }
    }

    pub fn mode(self, m: Mode) -> Self {
        P::gpio_regs().mode(self.idx, m);
        self
    }

    pub fn output_type(self, t: OutputType) -> Self {
        P::gpio_regs().output_type(self.idx, t);
        self
    }

    pub fn output_speed(self, s: OutputSpeed) -> Self {
        P::gpio_regs().output_speed(self.idx, s);
        self
    }

    pub fn pull(self, p: Pull) -> Self {
        P::gpio_regs().pull(self.idx, p);
        self
    }

    pub fn alt_function(self, a: u32) -> Self {
        P::gpio_regs().alt_function(self.idx, a);
        self
    }

    pub fn set(self) -> Self {
        P::gpio_regs().set(self.idx);
        self
    }

    pub fn reset(self) -> Self {
        P::gpio_regs().reset(self.idx);
        self
    }
}

pub trait GPIOPin {
    fn input(&self) -> bool;
    fn output(&mut self, v: bool);
    fn set(&mut self);
    fn reset(&mut self);
}

pub struct GPIO<P: GPIOPort> {
    idx: u8,
    _regs: PhantomData<*const P>,
}
impl<P: GPIOPort> GPIOPin for GPIO<P> {
    fn input(&self) -> bool {
        P::gpio_regs().read(self.idx)
    }

    fn output(&mut self, v: bool) {
        P::gpio_regs().write(self.idx, v);
    }

    fn set(&mut self) {
        P::gpio_regs().set(self.idx);
    }

    fn reset(&mut self) {
        P::gpio_regs().reset(self.idx);
    }
}
