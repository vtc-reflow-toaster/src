#![allow(dead_code)]

use core::ops::{Add, Sub};
use rt::exception;
use crate::util::mutex::Mutex;

const EPOCH_FREQ: u32 = 10;
const EPOCH_PERIOD_US: u32 = 1_000_000 / EPOCH_FREQ;
const EPOCH_PERIOD_MS: u32 = EPOCH_PERIOD_US / 1_000;
const COUNTER_RELOAD: u32 = crate::clock::CORE_FREQ / EPOCH_FREQ - 1;
const EPOCH_POSITIVE: u32 = i32::max_value() as u32;
/* SysTick RVR is limited to 24 bits and operates at CORE_FREQ.  Duration's ctr field is bounded by
 * this number, leaving a few extra bits.  We can improve its precision by defining a jiffy to be
 * 1/256 of a clock cycle.
 */
//const JIFFIES_PER_USEC: u32 = ((crate::clock::CORE_FREQ as u64 * 256) / 1_000_000) as u32;
const JIFFIES_PER_USEC: u32 = 256;
const JIFFIES_PER_COUNT: u32 = (1_000_000 * 256 / crate::clock::CORE_FREQ);

// EPOCH: track counter reloads to provide a continuous timer.
static mut EPOCH: Mutex<u32> = Mutex::new(0);

#[exception]
fn SysTick() {
    read_epoch();
}

pub fn init(stk: &mut cm::peripheral::SYST) {
    use cm::peripheral::syst;
    stk.set_clock_source(syst::SystClkSource::Core);
    stk.set_reload(COUNTER_RELOAD);
    stk.clear_current();
    stk.enable_interrupt();
    stk.enable_counter();
}

fn read_epoch() -> u32 {
    unsafe {
        EPOCH.enter(|e| {
            let wrapped = cm::Peripherals::steal().SYST.has_wrapped();
            if wrapped {
                *e += 1;
            }
            *e
        })
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Time {
    epoch: u32,
    ctr: u32,
}
impl Time {
    pub const fn empty() -> Time {
        Time {
            epoch: 0,
            ctr: 0,
        }
    }

    /// Get the current time
    pub fn now() -> Time {
        let mut epoch = read_epoch();
        loop {
            let ctr = mcu::SYST::get_current();
            let new_epoch = read_epoch();
            if new_epoch == epoch {
                break Time {
                    epoch,
                    ctr
                }
            }
            epoch = new_epoch;
        }
    }

    // XXX PartialOrd?
    pub fn pending(self) -> bool {
        let now = Time::now();
        now.epoch.wrapping_sub(self.epoch) > EPOCH_POSITIVE ||
            (now.epoch == self.epoch && now.ctr.wrapping_sub(self.ctr) < COUNTER_RELOAD)
    }

    pub fn passed(self) -> bool {
        !self.pending()
    }
}
impl Add<Duration> for Time {
    type Output = Time;

    fn add(self, r: Duration) -> Time {
        if self.ctr.wrapping_sub(r.ctr) > COUNTER_RELOAD {
            Time {
                epoch: self.epoch + r.epoch + 1,
                ctr: self.ctr + COUNTER_RELOAD - r.ctr,
            }
        } else {
            Time {
                epoch: self.epoch + r.epoch,
                ctr: self.ctr - r.ctr,
            }
        }
    }
}
impl Sub<Time> for Time {
    type Output = Duration;

    fn sub(self, r: Time) -> Duration {
        if self.epoch < r.epoch || (self.epoch == r.epoch && self.ctr > r.ctr) {
            Duration::from_secs(0)
        } else if self.ctr > r.ctr {
            Duration {
                epoch: self.epoch - r.epoch - 1,
                ctr: r.ctr + COUNTER_RELOAD - self.ctr,
            }
        } else {
            Duration {
                epoch: self.epoch - r.epoch,
                ctr: r.ctr - self.ctr,
            }
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Duration {
    epoch: u32,
    ctr: u32,
}
impl Duration {
    pub const fn from_usecs(usecs: u32) -> Duration {
        Duration {
            epoch: usecs / EPOCH_PERIOD_US,
            //ctr: (usecs % EPOCH_PERIOD_US) * 256 / JIFFIES_PER_USEC,
            ctr: (usecs % EPOCH_PERIOD_US) * JIFFIES_PER_USEC / JIFFIES_PER_COUNT,
        }
    }
    pub const fn from_msecs(msecs: u32) -> Duration {
        Duration {
            epoch: msecs / (EPOCH_PERIOD_US / 1_000),
            //ctr: ((msecs * 1_000) % EPOCH_PERIOD_US) * 256 / JIFFIES_PER_USEC,
            ctr: (msecs % EPOCH_PERIOD_MS) * 1_000 * JIFFIES_PER_USEC / JIFFIES_PER_COUNT,
        }
    }
    pub const fn from_secs(secs: u32) -> Duration {
        Duration {
            epoch: secs * EPOCH_FREQ,
            ctr: 0,
        }
    }

    pub const fn to_usecs(self) -> u32 {
        //self.epoch * EPOCH_PERIOD_US + self.ctr * JIFFIES_PER_USEC / 256
        self.epoch * EPOCH_PERIOD_US + self.ctr * JIFFIES_PER_COUNT / JIFFIES_PER_USEC
    }

    pub const fn to_msecs(self) -> u32 {
        //self.epoch * (EPOCH_PERIOD_US / 1_000) + self.ctr * JIFFIES_PER_USEC / 256_000
        self.epoch * EPOCH_PERIOD_MS + self.ctr * JIFFIES_PER_COUNT / (1_000 * JIFFIES_PER_USEC)
    }

    pub const fn to_secs(self) -> u32 {
        self.epoch / EPOCH_FREQ
    }
}
impl Add for Duration {
    type Output = Duration;

    fn add(self, r: Duration) -> Duration {
        if self.ctr + r.ctr > COUNTER_RELOAD {
            Duration {
                epoch: self.epoch + r.epoch + 1,
                ctr: self.ctr + r.ctr - COUNTER_RELOAD,
            }
        } else {
            Duration {
                epoch: self.epoch + r.epoch,
                ctr: self.ctr + r.ctr,
            }
        }
    }
}
