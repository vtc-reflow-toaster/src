use crate::systick::{Duration, Time};
use crate::gpio::GPIOPin;

#[derive(Debug, Copy, Clone, PartialEq)]
enum Phase {
    A,
    B,
    C,
    D,
}
impl Phase {
    fn next(self) -> Self {
        match self {
            Phase::A => Phase::B,
            Phase::B => Phase::C,
            Phase::C => Phase::D,
            Phase::D => Phase::A,
        }
    }

    fn prev(self) -> Self {
        match self {
            Phase::A => Phase::D,
            Phase::B => Phase::A,
            Phase::C => Phase::B,
            Phase::D => Phase::C,
        }
    }
}

pub struct StepperMotor<A, B, C, D> {
    a: A,
    b: B,
    c: C,
    d: D,

    next_step: Time,
    position: i32,
    target: i32,
    phase: Phase,
}

impl<A, B, C, D> StepperMotor<A, B, C, D> {
    /* 2000-2500 is about as fast as these motors will go.  5000 should be a safe value even
     * under load.
     */
    const STEP_TIME: Duration = Duration::from_usecs(5000);

    pub const fn new(a: A, b: B, c: C, d: D) -> StepperMotor<A, B, C, D> {
        StepperMotor {
            a, b, c, d,
            next_step: Time::empty(),
            position: 0,
            target: 0,
            phase: Phase::A,
        }
    }

    pub fn set_position(&mut self, position: i32) {
        self.target = position;
    }

    pub const fn get_position(&self) -> i32 {
        self.position
    }

    pub fn zero(&mut self) {
        self.target -= self.position;
        self.position = 0;
    }
}

impl<A: GPIOPin, B: GPIOPin, C: GPIOPin, D: GPIOPin> StepperMotor<A, B, C, D> {
    fn set_phase(&mut self, phase: Phase, val: bool) {
        match phase {
            Phase::A => self.a.output(val),
            Phase::B => self.b.output(val),
            Phase::C => self.c.output(val),
            Phase::D => self.d.output(val),
        }
    }

    pub fn poll(&mut self) {
        if !self.next_step.passed() {
            return;
        }

        if self.target > self.position {
            self.position += 1;
            let new_phase = self.phase.next();
            self.set_phase(new_phase, true);
            self.set_phase(self.phase, false);
            self.phase = new_phase;
        } else if self.target < self.position {
            self.position -= 1;
            let new_phase = self.phase.prev();
            self.set_phase(new_phase, true);
            self.set_phase(self.phase, false);
            self.phase = new_phase;
        } else if self.position == 0 {
            // If we're finished and in the home position, we don't need to keep the
            // coil energized.
            self.set_phase(self.phase, false);
        }

        self.next_step = self.next_step + Self::STEP_TIME;
    }
}
