// TODO: DMA to allow higher bandwidth

use crate::util::mutex::Mutex;
use common::define_ring_buffer;
use common::protocol;
use common::types::logging;
use common::ring_buffer::{RingBuffer, RingBuffer1024, RingBuffer32, RingBufferExtend};
use core::fmt;
use mcu::interrupt;

struct MessageWriter<'a> {
    level: logging::LogLevel,
    target: &'a str,
    buf: [u8; 224],
    idx: usize,
}
impl<'a> MessageWriter<'a> {
    fn new(level: logging::LogLevel, target: &'a str) -> MessageWriter<'a> {
        MessageWriter {
            level,
            target,
            buf: unsafe { core::mem::uninitialized() },
            idx: 0,
        }
    }
}
impl<'a> Drop for MessageWriter<'a> {
    fn drop(&mut self) {
        let _ = core::str::from_utf8(&self.buf[..self.idx])
            .map_err(|_| ())
            .and_then(|s| send(&protocol::CtrlMsg::Message(self.level, self.target, s)));
    }
}
impl<'a> fmt::Write for MessageWriter<'a> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        //let _ = heprintln!("Trying to log: {}", s);
        if s.len() > self.buf[self.idx..].len() {
            Err(fmt::Error {})
        } else {
            for (src, dst) in s.bytes().zip(self.buf[self.idx..].iter_mut()) {
                *dst = src;
            }
            self.idx += s.len();
            Ok(())
        }
    }
}

#[cfg(feature = "logging")]
pub fn log(level: logging::LogLevel, target: &str, args: fmt::Arguments) {
    // XXX probably shouldn't unwrap() this.
    fmt::write(&mut MessageWriter::new(level, target), args).unwrap();
}

#[cfg(not(feature = "logging"))]
pub fn log(level: logging::LogLevel, target: &str, args: fmt::Arguments) {}

#[allow(unused_macros)]
macro_rules! log {
    (target : $tgt:expr , $lvl:expr , $($arg:tt)+) => {
        $crate::message::log($lvl, $tgt, format_args!($($arg)+))
    };
    ($lvl:expr , $($arg:tt)+) => { log!(target: module_path!(), $lvl, $($arg)+) }
}

#[allow(unused_macros)]
macro_rules! error {
    (target : $tgt:expr , $(arg:tt)+) => { log!(target: $tgt, $crate::LogLevel::Error, $($arg)+) };
    ($($arg:tt)+) => { log!(target: module_path!(), $crate::LogLevel::Error, $($arg)+) }
}
#[allow(unused_macros)]
macro_rules! warn {
    (target : $tgt:expr , $(arg:tt)+) => { log!(target: $tgt, $crate::LogLevel::Warn, $($arg)+) };
    ($($arg:tt)+) => { log!(target: module_path!(), $crate::LogLevel::Warn, $($arg)+) }
}
#[allow(unused_macros)]
macro_rules! info {
    (target : $tgt:expr , $(arg:tt)+) => { log!(target: $tgt, $crate::LogLevel::Info, $($arg)+) };
    ($($arg:tt)+) => { log!(target: module_path!(), $crate::LogLevel::Info, $($arg)+) }
}
#[allow(unused_macros)]
macro_rules! debug {
    (target : $tgt:expr , $(arg:tt)+) => { log!(target: $tgt, $crate::LogLevel::Debug, $($arg)+) };
    ($($arg:tt)+) => { log!(target: module_path!(), $crate::LogLevel::Debug, $($arg)+) }
}
#[allow(unused_macros)]
macro_rules! trace {
    (target : $tgt:expr , $(arg:tt)+) => { log!(target: $tgt, $crate::LogLevel::Trace, $($arg)+) };
    ($($arg:tt)+) => { log!(target: module_path!(), $crate::LogLevel::Trace, $($arg)+) }
}

#[interrupt]
fn USART2() {
    let regs = unsafe { mcu::Peripherals::steal().USART2 };
    let isr = regs.isr.read();
    if isr.rxne().bit_is_set() {
        unsafe {
            let _ = ISR_DATA
                .enter(|rx| rx.rx_queue.push((regs.rdr.read().bits() & 0xff) as u8))
                .or_else(|_| send(&protocol::CtrlMsg::InputOverflow));
        }
    }
    if isr.ore().bit_is_set() {
        let _ = send(&protocol::CtrlMsg::InputOverflow);
    }
    regs.icr.write(|w| {
        w.wucf()
            .bit(isr.wuf().bit())
            .cmcf()
            .bit(isr.cmf().bit())
            .eobcf()
            .bit(isr.eobf().bit())
            .rtocf()
            .bit(isr.rtof().bit())
            .ctscf()
            .bit(isr.cts().bit())
            .lbdcf()
            .bit(isr.lbdf().bit())
            //.tcbgtcf().bit(isr.tcbgtf().bit())
            .tccf()
            .bit(isr.tc().bit())
            .idlecf()
            .bit(isr.idle().bit())
            .orecf()
            .bit(isr.ore().bit())
            .ncf()
            .bit(isr.nf().bit())
            .fecf()
            .bit(isr.fe().bit())
            .pecf()
            .bit(isr.pe().bit())
    });
}

// ISRData: information required to pass data from ISR to user
struct ISRData {
    rx_queue: RingBuffer32<u8>,
}
static mut ISR_DATA: Mutex<ISRData> = Mutex::new(ISRData {
    rx_queue: define_ring_buffer! { RingBuffer32: [0u8; 32] },
});

// TXData: information required to queue and dequeue data for transmission
struct TXData {
    tx_queue: RingBuffer1024<u8>,
    tx_overrun: bool,
}
static mut TX_DATA: Mutex<TXData> = Mutex::new(TXData {
    tx_queue: define_ring_buffer! { RingBuffer1024: [0u8; 1024] },
    tx_overrun: false,
});

pub fn send(msg: &protocol::CtrlMsg) -> Result<(), ()> {
    let mut buf: [u8; 256] = unsafe { core::mem::uninitialized() };
    //heprintln!("Logging {:?}", msg);
    // If we can't send a message, we probably can't send useful error messages...
    protocol::encode_message_to_slice(&mut buf, msg)
        .map_err(|_| ())
        .and_then(|s| unsafe { TX_DATA.enter(|tx| tx.tx_queue.extend(s)) })
}

pub struct Messenger<'a> {
    regs: mcu::USART2,
    decoder: protocol::MessageDecoder<
        protocol::WireSliceStorage<'a, u8>,
        protocol::WireSliceStorage<'a, usize>,
    >,
}
impl<'a> Messenger<'a> {
    // NB: caller is responsible for clocking USART2 at 32 MHz, enabling interrupt
    pub fn new(
        regs: mcu::USART2,
        dec_buf: &'a mut [u8],
        dec_idx: &'a mut [usize],
    ) -> Messenger<'a> {
        // 115200 baud, 8 bit, no parity
        regs.brr.write(|w| unsafe { w.bits(0x116) });
        // 57600 baud
        //regs.brr.write(|w| unsafe { w.bits(0x22c) });
        // 19200 baud
        //regs.brr.write(|w| unsafe { w.bits(0x683) });
        regs.cr1.modify(|_, w| {
            w.rxneie()
                .set_bit()
                .te()
                .set_bit()
                .re()
                .set_bit()
                .ue()
                .set_bit()
        });
        Messenger {
            regs,
            decoder: protocol::MessageDecoder::new(
                protocol::WireSliceStorage::new(dec_buf),
                protocol::WireSliceStorage::new(dec_idx),
            )
            .unwrap(),
        }
    }

    // XXX this should actually be atomic!
    // It might be worth doing reads here inside a single critical section and flagging when new
    // data arrives?
    pub fn poll(&mut self) {
        let txo = unsafe { TX_DATA.bypass().tx_overrun };
        let clear_txo = txo && send(&protocol::CtrlMsg::OutputOverflow).is_ok();
        let mut rx_buf: [u8; 32] = unsafe { core::mem::uninitialized() };
        let mut rx_cnt = 0usize;
        cm::interrupt::free(|cs| {
            let tx_data = unsafe { TX_DATA.borrow(cs) };
            if clear_txo {
                tx_data.tx_overrun = false;
            }

            if self.regs.isr.read().txe().bit_is_set() {
                if let Some(byte) = tx_data.tx_queue.pop() {
                    self.regs
                        .tdr
                        .write(|w| unsafe { w.tdr().bits(byte as u16) });
                }
            }

            /* FIXME: ring_buffer really needs to be lock-free.  COBS decoding isn't very
             * expensive, but it doesn't _need_ to be done inside the critical section.
             */
            let rx_iter = unsafe { ISR_DATA.borrow(cs).rx_queue.iter() };
            rx_cnt = rx_buf
                .iter_mut()
                .zip(rx_iter)
                .map(|(dst, byte)| {
                    *dst = byte;
                })
                .count();
        });
        if let Err(e) = self.decoder.decode(&rx_buf[..rx_cnt]) {
            error!("Error decoding message: {}", e);
        }
    }

    pub fn recv(&mut self) -> Option<protocol::HostMsg> {
        match self.decoder.recv() {
            Ok(msg) => msg,
            Err(e) => {
                error!("Malformed message: {}", e);
                None
            }
        }
    }
}
