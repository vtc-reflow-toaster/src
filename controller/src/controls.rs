use crate::gpio::GPIOPin;
use crate::stepper::StepperMotor;
use crate::message;
use common::operation::{AbstractControls, AbstractChannel};
use common::protocol::CtrlMsg;
use core::fmt;

pub struct Channel;
impl Channel {
    pub fn new() -> Self {
        Channel {}
    }
}
impl AbstractChannel for Channel {
    fn send(&mut self, msg: CtrlMsg) {
        let _ = message::send(&msg);
    }

    fn log(&mut self, args: fmt::Arguments) {
        use common::types::logging::LogLevel;
        message::log(LogLevel::Info, module_path!(), args);
    }
}

pub struct Controls<GT, GB, GF, SA, SB, SC, SD>
where
    GT: GPIOPin,
    GB: GPIOPin,
    GF: GPIOPin,
    SA: GPIOPin,
    SB: GPIOPin,
    SC: GPIOPin,
    SD: GPIOPin,
{
    // FIXME: just reread the output reg
    top_on: bool,
    bottom_on: bool,
    fan_on: bool,

    top_gpio: GT,
    bottom_gpio: GB,
    fan_gpio: GF,
    stepper: StepperMotor<SA, SB, SC, SD>,
}

impl<GT, GB, GF, SA, SB, SC, SD> Controls<GT, GB, GF, SA, SB, SC, SD>
where
    GT: GPIOPin,
    GB: GPIOPin,
    GF: GPIOPin,
    SA: GPIOPin,
    SB: GPIOPin,
    SC: GPIOPin,
    SD: GPIOPin,
{
    pub fn new(top_gpio: GT, bottom_gpio: GB, fan_gpio: GF, stepper: StepperMotor<SA, SB, SC, SD>) -> Self {
        Self {
            top_on: false,
            bottom_on: false,
            fan_on: false,

            top_gpio,
            bottom_gpio,
            fan_gpio,
            stepper,
        }
    }

    pub fn poll(&mut self) {
        self.stepper.poll();
    }
}

impl<GT, GB, GF, SA, SB, SC, SD> AbstractControls for Controls<GT, GB, GF, SA, SB, SC, SD>
where
    GT: GPIOPin,
    GB: GPIOPin,
    GF: GPIOPin,
    SA: GPIOPin,
    SB: GPIOPin,
    SC: GPIOPin,
    SD: GPIOPin,
{
    fn set_top_power(&mut self, power: bool) {
        self.top_on = power;
        self.top_gpio.output(power);
    }

    fn set_bottom_power(&mut self, power: bool) {
        self.bottom_on = power;
        self.bottom_gpio.output(power);
    }

    fn set_fan_power(&mut self, power: bool) {
        self.fan_on = power;
        self.fan_gpio.output(power);
    }

    fn set_door_position(&mut self, position: i32) {
        self.stepper.set_position(position);
    }

    fn zero_door(&mut self) {
        self.stepper.zero();
    }

    fn get_top_power(&self) -> bool {
        self.top_on
    }

    fn get_bottom_power(&self) -> bool {
        self.bottom_on
    }

    fn get_fan_power(&self) -> bool {
        self.fan_on
    }

    fn get_door_position(&self) -> i32 {
        self.stepper.get_position()
    }
}
