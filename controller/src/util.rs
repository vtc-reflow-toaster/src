#![allow(dead_code)]

pub mod mutex {
    use crate::cm::interrupt::{self, CriticalSection};
    use core::cell::UnsafeCell;

    pub struct Mutex<T>(UnsafeCell<T>);
    impl<T> Mutex<T> {
        pub const fn new(value: T) -> Self {
            Mutex(UnsafeCell::new(value))
        }

        #[allow(clippy::mut_from_ref)]
        pub fn borrow<'cs>(&'cs self, _cs: &'cs CriticalSection) -> &'cs mut T {
            unsafe { &mut *self.0.get() }
        }

        pub fn enter<U>(&self, f: impl FnOnce(&mut T) -> U) -> U {
            interrupt::free(|cs| f(self.borrow(cs)))
        }

        pub unsafe fn bypass(&self) -> &T {
            &*self.0.get()
        }
    }
}
