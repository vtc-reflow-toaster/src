#![no_std]
#![no_main]

extern crate cortex_m as cm;
extern crate cortex_m_rt as rt;
#[cfg(feature = "semihosting")]
extern crate panic_semihosting;
extern crate stm32l031k6 as mcu;
#[cfg(feature = "semihosting")]
#[allow(unused_imports)]
#[macro_use]
extern crate cortex_m_semihosting;

#[cfg(not(feature = "semihosting"))]
#[macro_use]
mod cortex_m_semihosting {
    // TODO: provide fallback implementations that trigger side effects
}

#[cfg(not(feature = "semihosting"))]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    cm::interrupt::disable();
    loop {}
}

pub use common::types::logging::LogLevel;
#[macro_use]
mod message;
mod clock;
mod controls;
mod gpio;
mod max31855;
mod spi;
mod stepper;
mod systick;
mod util;

use systick::{Duration, Time};
const OPERATION_INTERVAL: Duration = Duration::from_secs(1);

use common::operation::*;
use common::protocol::CtrlMsg;
use common::types::temperature;
use gpio::GPIOPort;
use max31855::ThermocoupleStatus;
use max31855::MAX31855;
use spi::SPI;

/* Pinout for controller board:
 * PA0 nCS1
 * PA1 nCS2
 * PA3 SM_A
 * PA4 SM_B
 * PA5 SCK
 * PA6 MISO
 * PA7 SM_C
 * PA9 REL_A (JP1)
 * PA10 REL_B (JP2)
 * PA12 SM_D
 * PB0 REL_C (JP3)
 */

use rt::entry;
#[entry]
fn main() -> ! {
    let mut periph = mcu::Peripherals::take().unwrap();
    let mut cm_periph = cm::Peripherals::take().unwrap();
    clock::init(&mut periph);

    periph
        .RCC
        .iopenr
        .modify(|_, w| w.iopaen().set_bit().iopben().set_bit());
    let gpioa = periph.GPIOA;
    let gpiob = periph.GPIOB;
    let spi1 = periph.SPI1;
    gpio::Builder::new(&gpioa, 2)
        .unwrap()
        .output_speed(gpio::OutputSpeed::VeryHigh)
        .alt_function(4)
        .mode(gpio::Mode::Alternate);
    gpio::Builder::new(&gpioa, 15)
        .unwrap()
        .alt_function(4)
        .mode(gpio::Mode::Alternate);

    periph
        .RCC
        .ccipr
        .modify(|_, w| w.usart2sel1().clear_bit().usart2sel0().set_bit()); // System clock for USART2
    periph.RCC.apb1enr.modify(|_, w| w.usart2en().set_bit());

    static mut MSG_DEC_BUF: [u8; 512] = [0; 512];
    static mut MSG_DEC_IDX: [usize; 16] = [0; 16];
    let mut messenger =
        message::Messenger::new(periph.USART2, unsafe { &mut MSG_DEC_BUF }, unsafe {
            &mut MSG_DEC_IDX
        });
    cm_periph.NVIC.enable(mcu::Interrupt::USART2);

    let phasea = gpio::Builder::new(&gpioa, 3)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();
    let phaseb = gpio::Builder::new(&gpioa, 4)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();
    let phasec = gpio::Builder::new(&gpioa, 7)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();
    let phased = gpio::Builder::new(&gpioa, 12)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();
    let stepper = stepper::StepperMotor::new(phasea, phaseb, phasec, phased);

    let rela = gpio::Builder::new(&gpioa, 9)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();
    let relb = gpio::Builder::new(&gpioa, 10)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();
    let relc = gpio::Builder::new(&gpiob, 0)
        .unwrap()
        .reset()
        .mode(gpio::Mode::Output)
        .get();

    let mut spi = SPI::new(
        gpio::Builder::new(&gpioa, 5).unwrap(),
        gpio::Builder::new(&gpioa, 6).unwrap(),
        spi1,
        &mut periph.RCC,
    );
    let mut max1 = max31855::MAX31855::new(gpio::Builder::new(&gpioa, 0).unwrap());
    let mut max2 = max31855::MAX31855::new(gpio::Builder::new(&gpioa, 1).unwrap());

    systick::init(&mut cm_periph.SYST);

    //let mut operation: Option<(Time, Time, Operation)> = None;
    let mut operation: Option<RunningOperation> = None;
    let mut controls = controls::Controls::new(relc, relb, rela, stepper);
    let mut channel = controls::Channel::new();

    let mut temp_report = TemperatureReporter::new();

    loop {
        messenger.poll();
        if let Some(msg) = messenger.recv() {
            use common::protocol::HostMsg;
            match msg {
                HostMsg::Hello => {
                    let _ = message::send(if operation.is_some() {
                        &CtrlMsg::Running
                    } else {
                        &CtrlMsg::Stopped
                    });
                    temp_report.connected(true);
                }

                HostMsg::Goodbye => {
                    temp_report.connected(false);
                }

                HostMsg::RunOperation(op) => {
                    if operation.is_none() {
                        operation = Some(RunningOperation::new(op, &mut controls));
                        let _ = message::send(&CtrlMsg::Running);
                        info!("Beginning operation");
                    } else {
                        error!("Operation already in progress");
                    }
                }

                HostMsg::AbortOperation => {
                    if let Some(op) = operation.as_mut() {
                        op.abort(&mut controls);
                    } else {
                        let _ = message::send(&CtrlMsg::Stopped);
                        error!("No operation in progress");
                    }
                }
            }
        }

        controls.poll();
        if let Some(op) = operation.as_mut() {
            op.poll(&mut spi, &mut max1, &mut max2, &mut controls, &mut channel);
            if op.finished() {
                let _ = message::send(&CtrlMsg::Stopped);
                operation = None;
            }
        }

        temp_report.poll(&mut spi, &mut max1);
    }
}

struct RunningOperation {
    began: Time,
    next_tick: Time,
    sensor_failed: bool,
    finished: bool,
    operation: Operation,
}
impl RunningOperation {
    pub fn new(mut operation: Operation, controls: &mut impl AbstractControls) -> RunningOperation {
        let now = systick::Time::now();
        operation.start(controls);
        RunningOperation {
            began: now,
            next_tick: now,
            sensor_failed: false,
            finished: false,
            operation,
        }
    }

    fn duration(&self) -> temperature::Interval {
        temperature::Interval::from_millis((Time::now() - self.began).to_msecs() as i32)
    }

    fn fallback_state(&self) -> OperationState {
        let duration = self.duration();
        OperationState {
            duration,
            temperature1: temperature::Temperature::new(0),
            temperature2: None,
        }
    }

    fn get_state<GA: GPIOPort, GB: GPIOPort>(
        &mut self,
        spi: &mut SPI,
        max1: &mut MAX31855<GA>,
        max2: &mut MAX31855<GB>,
    ) -> Result<OperationState, ()> {
        if self.sensor_failed {
            Err(())
        } else {
            let duration = self.duration();
            let res = max1.read(spi);

            let temp2 = match max2.read(spi).0 {
                ThermocoupleStatus::Success(t) => Some(t),
                _ => None,
            };

            match res.0 {
                ThermocoupleStatus::Success(temp) => Ok(OperationState {
                    duration,
                    temperature1: temp,
                    temperature2: temp2,
                }),

                status => {
                    error!(
                        "{}",
                        match status {
                            ThermocoupleStatus::ShortVCC => "Thermocouple shorted to VCC",
                            ThermocoupleStatus::ShortGND => "Thermocouple shorted to ground",
                            ThermocoupleStatus::Open => "Thermocouple not connected",
                            ThermocoupleStatus::Unknown => "Unknown conversion error",
                            _ => unreachable!(),
                        }
                    );
                    Err(())
                }
            }
        }
    }

    fn poll<GA: GPIOPort, GB: GPIOPort>(
        &mut self,
        spi: &mut SPI,
        max1: &mut MAX31855<GA>,
        max2: &mut MAX31855<GB>,
        controls: &mut impl AbstractControls,
        channel: &mut controls::Channel,
    ) {
        if self.finished {
            return;
        }
        if self.next_tick.pending() {
            return;
        }

        let state = self.get_state(spi, max1, max2).unwrap_or_else(|_| {
            if !self.sensor_failed {
                self.sensor_failed = true;
                self.operation.abort(controls);
            }
            self.fallback_state()
        });
        let result = self.operation.run(&state, controls, channel);
        if result == Completed::Complete {
            self.finished = true;
            info!("Operation finished");
        } else {
            self.next_tick = self.next_tick + OPERATION_INTERVAL;
        }
    }

    fn abort(&mut self, controls: &mut impl AbstractControls) {
        self.operation.abort(controls);
        info!("Operation aborted");
    }

    fn finished(&self) -> bool {
        self.finished
    }
}

struct TemperatureReporter {
    next_report: Option<Time>,
}

impl TemperatureReporter {
    fn new() -> TemperatureReporter {
        TemperatureReporter { next_report: None }
    }

    fn connected(&mut self, connected: bool) {
        self.next_report = if connected {
            Some(Time::now())
        } else {
            None
        }
    }

    fn poll<T: GPIOPort>(&mut self, spi: &mut SPI, max: &mut MAX31855<T>) {
        if let Some(next) = self.next_report {
            if next.passed() {
                let temp = match max.read(spi).0 {
                    ThermocoupleStatus::Success(temp) => Some(temp),
                    _ => None,
                };
                let _ = message::send(&CtrlMsg::Temperature(temp));
                self.next_report = Some(next + Duration::from_secs(1));
            }
        }
    }
}

#[cfg(test)]
mod test {}
