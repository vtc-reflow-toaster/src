// NB: conversion every 70-100ms
use common::types::temperature::Temperature;
use crate::gpio::{self, GPIOPin};
use crate::spi::SPI;

pub enum ThermocoupleStatus {
    Success(Temperature),
    Open,
    ShortVCC,
    ShortGND,
    Unknown
}

// External and internal temperatures
// FIXME: getters?
pub struct ConversionResult(pub ThermocoupleStatus, pub Temperature);

pub struct MAX31855<PORT: gpio::GPIOPort> {
    cs: gpio::GPIO<PORT>
}

impl<PORT: gpio::GPIOPort> MAX31855<PORT> {
    pub fn new(gpio: gpio::Builder<PORT>) -> MAX31855<PORT> {
        MAX31855 {
            cs: gpio
                .output_speed(gpio::OutputSpeed::VeryHigh)
                .set()
                .mode(gpio::Mode::Output)
                .get()
        }
    }

    pub fn read(&mut self, spi: &mut SPI) -> ConversionResult {
        // FIXME: gpio access should really be dynamically dispatched
        self.cs.reset();
        let word: u32 = spi.read_int();
        self.cs.set();

        let reference = (((word & (((1 << 12) -1) << 4)) as i16) >> 4) as i32;
        let reftemp = Temperature::new_prescaled(reference * (Temperature::SCALE / 16));

        if word & (1 << 16) == 0 {
            let junction = (((word >> 16) as i16) >> 2) as i32;
            let jcttemp = Temperature::new_prescaled(junction * (Temperature::SCALE / 4));

            ConversionResult(ThermocoupleStatus::Success(jcttemp), reftemp)
        } else {
            if word & (1 << 2) != 0 {
                ConversionResult(ThermocoupleStatus::ShortVCC, reftemp)
            } else if word & (1 << 1) != 0 {
                ConversionResult(ThermocoupleStatus::ShortGND, reftemp)
            } else if word & (1 << 0) != 0 {
                ConversionResult(ThermocoupleStatus::Open, reftemp)
            } else {
                ConversionResult(ThermocoupleStatus::Unknown, reftemp)
            }
        }
    }
}
