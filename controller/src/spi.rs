use crate::gpio;
use core::ops::{BitOr, Shl};

pub struct SPI(mcu::SPI1);

impl SPI {
    //pub fn new(periph: &mut mcu::Peripherals) -> SPI {
    pub fn new<SCKP: gpio::GPIOPort, MISOP: gpio::GPIOPort>(sck: gpio::Builder<SCKP>,
                            miso: gpio::Builder<MISOP>,
                            spi: mcu::SPI1,
                            rcc: &mut mcu::RCC) -> SPI {
        rcc.iopenr.modify(|_, w| w.iopaen().set_bit());
        sck
            .output_speed(gpio::OutputSpeed::VeryHigh)
            .mode(gpio::Mode::Alternate)
            .alt_function(0);
        miso
            .output_speed(gpio::OutputSpeed::VeryHigh)
            .mode(gpio::Mode::Alternate)
            .alt_function(0);

        rcc.apb2enr.modify(|_, w| w.spi1en().set_bit());
        spi.cr1.write(|w| unsafe {
            w.br().bits(0b111)
                .rxonly().set_bit()
                .ssm().set_bit()
                .ssi().set_bit()
                .mstr().set_bit()
        });

        SPI(spi)
    }

    pub fn read_int<T>(&mut self) -> T
        where T: Sized + BitOr<T, Output=T> + Shl<usize, Output=T> + From<u8> {
        let mut t: T = T::from(0u8);
        self.0.cr1.modify(|_, w| w.spe().set_bit());
        for _ in 0..core::mem::size_of::<T>() {
            while self.0.sr.read().rxne().bit_is_clear() {
                // wait...
            }
            //t = (t << 8) | (spi.dr.read().bits() as T & T::from(0xffu8));
            t = (t << 8) | T::from((self.0.dr.read().bits() & 0xff) as u8);
        }
        self.0.cr1.modify(|_, w| w.spe().clear_bit());

        t
    }
}
