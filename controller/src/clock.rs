#[allow(dead_code)]
pub const CORE_FREQ: u32 = 32_000_000;

// NB: the old stepper code was running at 4 MHz!
pub fn init(periph: &mut mcu::Peripherals) {
    // Loosely transcribed from ST's generated HAL stuff

    periph.PWR.cr.modify(|_, w| unsafe { w.vos().bits(0b01) }); // 1.8 V power (up to 32MHz)

    periph
        .RCC
        .icscr
        .modify(|_, w| unsafe { w.hsi16trim().bits(0x10) });
    periph
        .RCC
        .cr
        .modify(|_, w| w.hsi16on().set_bit().hsi16diven().clear_bit());
    while periph.RCC.cr.read().hsi16rdyf().bit_is_clear() {}

    // Assumption: sysclk is not running on the pll at boot
    periph.RCC.cr.modify(|_, w| w.pllon().clear_bit());
    while periph.RCC.cr.read().pllrdy().bit_is_set() {
        // Wait for PLL to disable
    }
    periph.RCC.cfgr.modify(|_, w| unsafe {
        w.pllsrc()
            .clear_bit() // PLL mux -> HSI
            .pllmul()
            .bits(1) // PLL -> x4
            .plldiv()
            .bits(1) // PLL -> /2
    });
    periph.RCC.cr.modify(|_, w| w.pllon().set_bit());
    while periph.RCC.cr.read().pllrdy().bit_is_clear() {
        // Wait for PLL to lock
    }

    // The HAL doesn't enable the wait state, but the docs say it should?
    periph.FLASH.acr.modify(|_, w| w.latency().set_bit());
    while periph.FLASH.acr.read().latency().bit_is_clear() {
        // Should only take one read to take effect.
    }

    periph
        .RCC
        .cfgr
        .modify(|_, w| unsafe { w.hpre().bits(0b0000) }); // AHB prescale -> /1
                                                          // HAL rechecks PLL here
    periph.RCC.cfgr.modify(|_, w| unsafe { w.sw().bits(0b11) }); // Sys clock mux -> PLL
    while periph.RCC.cfgr.read().sws().bits() != 0b11 {
        // Wait for switch to PLL
    }

    periph.RCC.cfgr.modify(|_, w| unsafe {
        w.ppre1()
            .bits(0b110) // APB1 -> /8
            .ppre2()
            .bits(0b110) // APB2 -> /8
    });
}
