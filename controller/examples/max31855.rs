#![no_std]
#![no_main]

extern crate cortex_m as cm;
extern crate cortex_m_rt as rt;
extern crate panic_semihosting;
extern crate stm32l031k6 as mcu;
#[allow(unused_imports)]
#[macro_use]
extern crate cortex_m_semihosting;

#[path = "../src/clock.rs"]
mod clock;
#[path = "../src/gpio.rs"]
mod gpio;

use rt::entry;

#[entry]
fn main() -> ! {
    let mut periph = mcu::Peripherals::take().unwrap();
    clock::init(&mut periph);

    periph.RCC.iopenr.modify(|_, w| w.iopaen().set_bit());
    let gpioa = periph.GPIOA;
    // SCK
    gpio::Builder::new(&gpioa, 5)
        .unwrap()
        .output_speed(gpio::OutputSpeed::VeryHigh)
        .mode(gpio::Mode::Alternate)
        .alt_function(0);
    // MISO
    gpio::Builder::new(&gpioa, 6)
        .unwrap()
        .output_speed(gpio::OutputSpeed::VeryHigh)
        .mode(gpio::Mode::Alternate)
        .alt_function(0);
    // CSb
    let mut cs = gpio::Builder::new(&gpioa, 7)
        .unwrap()
        .output_speed(gpio::OutputSpeed::VeryHigh)
        .set()
        .mode(gpio::Mode::Output)
        .get();

    periph.RCC.apb2enr.modify(|_, w| w.spi1en().set_bit());
    let spi = periph.SPI1;
    spi.cr1.write(|w| unsafe {
        w.br().bits(0b111)
            //.cpol().clear_bit()
            //.cpha().clear_bit()
            .rxonly().set_bit()
            //.lsbfirst().clear_bit()
            //.crcen().clear_bit()
            .ssm().set_bit()
            .ssi().set_bit()
            .mstr().set_bit()
            //.dff().clear_bit() // 16-bit might actually be nice here?
    });
    // Either set SSOE or SSM & SSI to enable clock, apparently.
    //spi.cr2.write(|w| w.ssoe().set_bit());

    loop {
        let r = get_max_report(&mut cs, &spi);
        let _ = heprintln!("Report: Junction: {}C, Reference: {}C", r.junction / 4, r.reference/ 16);
        let _ = heprintln!("Fault: {}, SVCC: {}, SGND: {}, Open: {}",
                   r.fault, r.short_vcc, r.short_gnd, r.open);
    }
}

#[derive(Debug)]
struct MAXReport {
    pub fault: bool,
    pub short_vcc: bool,
    pub short_gnd: bool,
    pub open: bool,
    pub junction: i16, // 14 bits, 1/4 C
    pub reference: i16, // 12 bits, 1/16 C
}

#[allow(dead_code)]
fn get_max_report(cs: &mut impl gpio::GPIOPin, spi: &mcu::SPI1) -> MAXReport {
    cs.reset();

    let mut acc = 0u32;
    let mut reads = 0u32;

    spi.cr1.modify(|_, w| w.spe().set_bit());
    while reads < 4 {
        if spi.sr.read().rxne().bit_is_set() {
            acc = (acc << 8) | (spi.dr.read().bits() as u32 & 0xff);
            reads += 1;
        }
    }
    spi.cr1.modify(|_, w| w.spe().clear_bit());

    cs.set();

    MAXReport {
        fault: acc & (1 << 16) != 0,
        short_vcc: acc & (1 << 2) != 0,
        short_gnd: acc & (1 << 1) != 0,
        open: acc & (1 << 0) != 0,
        junction: ((acc >> 16) as i16) >> 2,
        reference: ((acc & (((1 << 12) - 1) << 4)) as i16) >> 4,
    }
}
