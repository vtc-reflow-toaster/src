#![no_std]
#![no_main]

extern crate cortex_m as cm;
extern crate cortex_m_rt as rt;
#[cfg(feature = "semihosting")]
extern crate panic_semihosting;
extern crate stm32l031k6 as mcu;

use cm::peripheral::syst;
use rt::entry;

#[path = "../src/clock.rs"]
mod clock;

const ITER_COUNT: u32 = clock::CLOCK_FREQ / 1_000_000;

#[cfg(not(feature = "semihosting"))]
#[panic_handler]
fn panic(_: &core::panic::PanicInfo) -> ! {
    loop {}
}

#[entry]
fn main() -> ! {
    let mut periph = mcu::Peripherals::take().unwrap();
    clock::init(&mut periph);

    periph.RCC.iopenr.modify(|_, w| w.iopben().set_bit());
    let _ = periph.RCC.iopenr.read().iopben().bit();

    periph
        .GPIOB
        .ospeedr
        .modify(|_, w| unsafe { w.ospeed3().bits(0b10) }); // High speed why not
    periph.GPIOB.otyper.modify(|_, w| w.ot3().clear_bit()); // Push-pull default
    periph
        .GPIOB
        .moder
        .modify(|_, w| unsafe { w.mode3().bits(0b01) }); // GPIO output
    periph
        .GPIOB
        .pupdr
        .modify(|_, w| unsafe { w.pupd3().bits(0b00) }); // No pull

    let cm_periph = cm::Peripherals::take().unwrap();
    let mut systick = cm_periph.SYST;
    systick.set_clock_source(syst::SystClkSource::Core);
    systick.set_reload(1_000_000);
    systick.clear_current();
    systick.enable_counter();

    loop {
        periph.GPIOB.bsrr.write(|w| w.bs3().set_bit());
        for _ in 0..ITER_COUNT {
            while !systick.has_wrapped() {}
        }
        periph.GPIOB.bsrr.write(|w| w.br3().set_bit());
        for _ in 0..ITER_COUNT {
            while !systick.has_wrapped() {}
        }
    }
}
