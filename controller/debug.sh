#!/bin/bash -m

#openocd -f st_nucleo_l0.cfg &
openocd -f openocd.cfg &
sleep 2
arm-none-eabi-gdb "$1" \
  -ex 'source gdb.rc' \
  -ex load

pkill -P "$$"
wait
