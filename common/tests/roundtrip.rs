extern crate quickcheck;
#[macro_use(quickcheck)]
extern crate quickcheck_macros;

extern crate common;
use common::protocol::wire;
use wire::WireStorage;

#[quickcheck]
fn roundtrip_slice(s: Vec<u8>) -> bool {
    let mut enc_buf: Vec<u8> = Vec::new();
    // wire encoding overhead: input * 255/254 (cobs) + 1 (null) + 1 (cksum) + 1 (length if no
    // zero bytes)
    //
    // This should be sufficient for all possible inputs:
    enc_buf.resize((s.len() * 255 / 254 + 4).max(4), 0);

    let mut dec_buf: Vec<u8> = Vec::new();
    dec_buf.resize(s.len() + 2, 0);
    let mut dec_idx = [0usize; 2];

    let mut enc = wire::WireEncoder::new(wire::WireSliceStorage::new(enc_buf.as_mut_slice()))
        .expect("Allocate encoder");
    let mut dec = wire::WireDecoder::new(
        wire::WireSliceStorage::new(dec_buf.as_mut_slice()),
        wire::WireSliceStorage::new(&mut dec_idx),
    )
    .expect("Allocate decoder");

    for b in s.iter() {
        enc.add_byte(*b).expect("Add bytes to encoder");
    }
    let cobs = enc.finish();
    for b in cobs.as_slice().iter() {
        dec.add_byte(*b).expect("Add bytes to decoder");
    }
    if let Some(frame) = dec.next_frame() {
        if frame != s.as_slice() {
            println!("In:   {:?}", s.as_slice());
            println!("COBS: {:?}", cobs.as_slice());
            println!("Out:  {:?}", frame);
            println!("");
        }
        frame == s.as_slice()
    } else {
        false
    }
}

#[quickcheck]
fn roundtrip_slices(vecs: Vec<Vec<u8>>) -> bool {
    let mut buf: Vec<u8> = Vec::new();

    for s in vecs.iter() {
        let mut enc_buf: Vec<u8> = Vec::new();
        enc_buf.resize((s.len() * 255 / 254 + 4).max(4), 0);

        let mut enc =
            wire::WireEncoder::new(wire::WireSliceStorage::new(enc_buf.as_mut_slice())).unwrap();
        for b in s.iter() {
            enc.add_byte(*b).unwrap();
        }
        buf.extend(enc.finish().as_slice());
    }

    let mut dec_buf: Vec<u8> = Vec::new();
    dec_buf.resize(buf.len() + 20, 0);
    let mut dec_idx: Vec<usize> = Vec::new();
    dec_idx.resize(vecs.len() + 2, 0);

    let mut dec = wire::WireDecoder::new(
        wire::WireSliceStorage::new(&mut dec_buf),
        wire::WireSliceStorage::new(&mut dec_idx),
    )
    .unwrap();
    for &b in buf.iter() {
        dec.add_byte(b).unwrap();
    }

    let mut siter = vecs.iter();
    loop {
        let dslice = dec.next_frame();
        let sslice = siter.next().map(|v| v.as_slice());
        if dslice != sslice {
            println!("In:   {:?}", sslice);
            println!("COBS: {:?}", buf.as_slice());
            println!("Out:  {:?}", dslice);
            println!("");
            return false;
        }
        if dslice == None {
            break;
        }
    }

    true
}

#[quickcheck]
fn roundtrip_slice_vec(s: Vec<u8>) -> bool {
    let mut enc = wire::WireEncoder::new(Vec::new()).expect("Allocate encoder");
    let mut dec = wire::WireDecoder::new(Vec::new(), Vec::new()).expect("Allocate decoder");

    for b in s.iter() {
        enc.add_byte(*b).expect("Add bytes to encoder");
    }
    let cobs = enc.finish();
    for b in cobs.iter() {
        dec.add_byte(*b).expect("Add bytes to decoder");
    }
    if let Some(frame) = dec.next_frame() {
        if frame != s.as_slice() {
            println!("In:   {:?}", s.as_slice());
            println!("COBS: {:?}", cobs);
            println!("Out:  {:?}", frame);
            println!("");
        }
        frame == s.as_slice()
    } else {
        false
    }
}

#[quickcheck]
fn roundtrip_slices_vec(vecs: Vec<Vec<u8>>) -> bool {
    let mut buf: Vec<u8> = Vec::new();

    for s in vecs.iter() {
        let mut enc = wire::WireEncoder::new(Vec::new()).unwrap();
        for b in s.iter() {
            enc.add_byte(*b).unwrap();
        }
        buf.extend(enc.finish());
    }

    let mut dec = wire::WireDecoder::new(Vec::new(), Vec::new()).unwrap();
    for &b in buf.iter() {
        dec.add_byte(b).unwrap();
    }

    let mut siter = vecs.iter();
    loop {
        let dslice = dec.next_frame();
        let sslice = siter.next().map(|v| v.as_slice());
        if dslice != sslice {
            println!("In:   {:?}", sslice);
            println!("COBS: {:?}", buf.as_slice());
            println!("Out:  {:?}", dslice);
            println!("");
            return false;
        }
        if dslice == None {
            break;
        }
    }

    true
}

// TODO: test the effectiveness of the checksumming, etc. against random perturbations.
