// FIXME: this all needs to be rewritten
type RBIndex = usize;

pub trait Storage: Sized {
    type Item;
    // Reader head, reader tail, buffer size
    fn indices(&self) -> (RBIndex, RBIndex, RBIndex);
    fn indices_mut(&mut self) -> (&mut RBIndex, &mut RBIndex, RBIndex);
    fn get(&mut self, idx: RBIndex) -> Self::Item;
    fn set(&mut self, idx: RBIndex, val: Self::Item);
}

pub struct FullIter<'a, S: Storage> {
    storage: &'a mut S,
    idx: RBIndex,
    brk: RBIndex,
    end: RBIndex,
}
impl<'a, S: Storage> FullIter<'a, S> {
    fn new(storage: &'a mut S) -> FullIter<'a, S> {
        let (head, tail, size) = storage.indices();
        FullIter {
            storage,
            idx: head,
            brk: size,
            end: tail,
        }
    }
}
impl<'a, S: Storage> Iterator for FullIter<'a, S> {
    type Item = S::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx == self.end {
            None
        } else {
            let idx = self.idx;
            self.idx = if self.idx + 1 == self.brk { 0 } else { self.idx + 1 };
            Some(self.storage.get(idx))
        }
    }
}
impl<'a, S: Storage> Drop for FullIter<'a, S> {
    fn drop(&mut self) {
        let (head, _, _) = self.storage.indices_mut();
        *head = self.idx;
    }
}

pub trait RingBuffer: Storage {
    fn can_push(&self, n: usize) -> bool {
        let (head, tail, end) = self.indices();
        let delta = if head <= tail { end } else { 0 } + head - tail;
        n < end as usize && n < delta as usize
    }

    fn push(&mut self, val: Self::Item) -> Result<(), ()> {
        if self.can_push(1) {
            let (_, tail, end) = self.indices_mut();
            let idx = *tail;
            *tail = if idx + 1 < end { idx + 1 } else { 0 };
            self.set(idx, val);
            Ok(())
        } else {
            Err(())
        }
    }

    fn can_pop(&self, n: usize) -> bool {
        let (head, tail, end) = self.indices();
        let delta = if tail < head { end } else { 0 } + tail - head;
        n <= end as usize && n <= delta as usize
    }

    fn pop(&mut self) -> Option<Self::Item> {
        if self.can_pop(1) {
            let (head, _, end) = self.indices_mut();
            let idx = *head;
            *head = if idx + 1 < end { idx + 1 } else { 0 };
            Some(self.get(idx))
        } else {
            None
        }
    }

    fn iter(&mut self) -> FullIter<Self> {
        FullIter::new(self)
    }

    fn clear(&mut self) {
        self.iter().count();
    }
}

pub trait RingBufferExtend<T: Clone>: RingBuffer + Storage<Item = T> {
    fn extend(&mut self, slice: &[Self::Item]) -> Result<(), ()> {
        if self.can_push(slice.len()) {
            let (_, tail, size) = self.indices();
            if tail + slice.len() < size {
                // We will not wrap
                for (off, item) in slice.iter().enumerate() {
                    self.set(tail + off as RBIndex, item.clone());
                }
                let (_, tail, _) = self.indices_mut();
                *tail += slice.len();
            } else {
                // We will wrap; split the slice in two
                let top_len = size - tail;
                let (top, bottom) = slice.split_at(top_len);

                for (off, item) in top.iter().enumerate() {
                    self.set(tail + off as RBIndex, item.clone());
                }
                for (off, item) in bottom.iter().enumerate() {
                    self.set(off as RBIndex, item.clone());
                }

                let (_, tail, _) = self.indices_mut();
                *tail = bottom.len();
            }

            Ok(())
        } else {
            Err(())
        }
    }
}

declare_ring_buffer!(RingBuffer4, 4);
declare_ring_buffer!(RingBuffer8, 8);
declare_ring_buffer!(RingBuffer16, 16);
declare_ring_buffer!(RingBuffer32, 32);
declare_ring_buffer!(RingBuffer64, 64);
declare_ring_buffer!(RingBuffer128, 128);
declare_ring_buffer!(RingBuffer256, 256);
declare_ring_buffer!(RingBuffer512, 512);
declare_ring_buffer!(RingBuffer1024, 1024);
declare_ring_buffer!(RingBuffer2048, 2048);
declare_ring_buffer!(RingBuffer4096, 4096);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn rb_works() {
        #[allow(dead_code)]
        static mut RB16: RingBuffer16<u8> = define_ring_buffer! { RingBuffer16: [0u8; 16] };

        let mut rb4 = define_ring_buffer! { RingBuffer4: [0u8; 4] };

        assert_eq!(rb4.can_pop(0), true);
        assert_eq!(rb4.can_pop(1), false);
        assert_eq!(rb4.can_push(3), true);
        assert_eq!(rb4.can_push(4), false);
        rb4.push(0).unwrap();
        rb4.push(1).unwrap();
        assert_eq!(rb4.can_pop(2), true);
        assert_eq!(rb4.can_pop(3), false);
        assert_eq!(rb4.can_push(1), true);
        assert_eq!(rb4.can_push(2), false);
        rb4.push(2).unwrap();
        assert!(rb4.push(3).is_err());
        assert_eq!(rb4.can_pop(3), true);
        assert_eq!(rb4.can_pop(4), false);
        assert_eq!(rb4.pop(), Some(0));
        assert_eq!(rb4.pop(), Some(1));
        assert_eq!(rb4.can_pop(1), true);
        assert_eq!(rb4.can_pop(2), false);
        assert_eq!(rb4.pop(), Some(2));
        assert_eq!(rb4.can_pop(0), true);
        assert_eq!(rb4.can_pop(1), false);

        assert_eq!(rb4.can_pop(0), true);
        assert_eq!(rb4.can_pop(1), false);
        assert_eq!(rb4.can_push(3), true);
        assert_eq!(rb4.can_push(4), false);
        rb4.push(0).unwrap();
        rb4.push(1).unwrap();
        assert_eq!(rb4.can_pop(2), true);
        assert_eq!(rb4.can_pop(3), false);
        assert_eq!(rb4.can_push(1), true);
        assert_eq!(rb4.can_push(2), false);
        rb4.push(2).unwrap();
        assert!(rb4.push(3).is_err());
        assert_eq!(rb4.can_pop(3), true);
        assert_eq!(rb4.can_pop(4), false);
        assert_eq!(rb4.pop(), Some(0));
        assert_eq!(rb4.pop(), Some(1));
        assert_eq!(rb4.can_pop(1), true);
        assert_eq!(rb4.can_pop(2), false);
        assert_eq!(rb4.can_push(2), true);
        assert_eq!(rb4.can_push(3), false);
        rb4.push(3).unwrap();
        rb4.push(4).unwrap();
        assert!(rb4.push(5).is_err());
    }

    #[test]
    fn iter_extend_works() {
        let mut rb4 = define_ring_buffer! { RingBuffer4: [0u8; 4] };
        eprintln!("{}, {}, {:?}", rb4.0, rb4.1, rb4.2);
        rb4.extend(&[0, 1, 2]).unwrap();
        eprintln!("{}, {}, {:?}", rb4.0, rb4.1, rb4.2);
        {
            let mut iter = rb4.iter();
            assert_eq!(iter.next(), Some(0));
            assert_eq!(iter.next(), Some(1));
            //assert_eq!(iter.next(), None);
        }
        eprintln!("{}, {}, {:?}", rb4.0, rb4.1, rb4.2);
        rb4.extend(&[3, 4]).unwrap();
        eprintln!("{}, {}, {:?}", rb4.0, rb4.1, rb4.2);
        {
            let mut iter = rb4.iter();
            assert_eq!(iter.next(), Some(2));
            assert_eq!(iter.next(), Some(3));
            assert_eq!(iter.next(), Some(4));
            assert_eq!(iter.next(), None);
        }
    }

    fn whine(rb16: &RingBuffer16<u8>, op: &str, num: usize, of: usize) {
        eprintln!("{} {} of {} from RB16({}, {}, ...)",
            op, num, of, rb16.0, rb16.1, /*rb16.2*/);
    }

    #[test]
    fn random_push_pop() {
        use rand::Rng;

        let mut rng = rand::thread_rng();
        let mut rb16 = define_ring_buffer! { RingBuffer16: [0u8; 16] };
        for _ in 0..100 {
            let mut free = 15;
            for _ in 0..100 {
                let fill = rng.gen_range(0, free + 1);
                let drain = rng.gen_range(0, 16 + fill - free);

                whine(&rb16, "push", fill, free);
                for _ in 0..fill {
                    rb16.push(1).unwrap();
                }
                whine(&rb16, "pop", drain, 15 + fill - free);
                for _ in 0..drain {
                    assert_eq!(rb16.pop(), Some(1));
                }

                free -= fill;
                free += drain;
            }

            whine(&rb16, "pop", 15 - free, 15 - free);
            for _ in 0..15 - free {
                assert_eq!(rb16.pop(), Some(1));
            }
        }
    }


    #[test]
    fn random_extend_take() {
        use rand::Rng;

        let mut rng = rand::thread_rng();
        let mut rb16 = define_ring_buffer! { RingBuffer16: [0u8; 16] };
        for _ in 0..100 {
            let mut free = 15;
            for _ in 0..100 {
                let fill = rng.gen_range(0, free + 1);
                let drain = rng.gen_range(0, 16 + fill - free);

                let vec = [1u8; 16];
                whine(&rb16, "extend", fill, free);
                rb16.extend(&vec[0..fill]).unwrap();

                whine(&rb16, "take", drain, 15 + fill - free);
                assert_eq!(rb16.iter().take(drain).count(), drain);

                free -= fill;
                free += drain;
            }

            whine(&rb16, "take", 15 - free, 15 - free);
            assert_eq!(rb16.iter().take(15 - free).count(), 15 - free);
        }
    }

    static mut DID_DROP: bool = false;

    struct DropWhine();
    impl DropWhine {
        fn new() -> DropWhine {
            eprintln!("Constructed new DropWhine");
            DropWhine()
        }
    }
    impl Drop for DropWhine {
        fn drop(&mut self) {
            eprintln!("Dropped a DropWhine!");
            unsafe {
                DID_DROP = true;
            }
        }
    }

    #[test]
    fn should_not_drop() {
        let mut rb = define_ring_buffer! { RingBuffer8: [DropWhine(); 8] };
        rb.push(DropWhine::new()).unwrap();
        rb.push(DropWhine::new()).unwrap();
        rb.push(DropWhine::new()).unwrap();
        core::mem::forget(rb.pop());
        core::mem::forget(rb.pop());
        rb.push(DropWhine::new()).unwrap();
        rb.push(DropWhine::new()).unwrap();
        core::mem::forget(rb.pop());
        core::mem::forget(rb.pop());
        rb.push(DropWhine::new()).unwrap();
        rb.push(DropWhine::new()).unwrap();
        core::mem::forget(rb.pop());
        core::mem::forget(rb.pop());
        core::mem::forget(rb.pop());
        unsafe {
            assert_eq!(DID_DROP, false);
        }

        // These _should_ drop:
        rb.push(DropWhine::new()).unwrap();
        rb.push(DropWhine::new()).unwrap();
        rb.clear();
        unsafe {
            assert_eq!(DID_DROP, true);
        }
    }
}
