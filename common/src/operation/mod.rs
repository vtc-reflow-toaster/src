/// Operations:
/// objects that can be passed to a simulation or sent over the wire to the controller for
/// execution
use crate::protocol::CtrlMsg;
use crate::types::temperature::*;
use enum_dispatch::enum_dispatch;
use serde::{Deserialize, Serialize};
use core::fmt;

pub mod jog_door;
use jog_door::JogDoor;

pub mod bang_bang_control;
use bang_bang_control::BangBangControl;

pub mod calibration;
use calibration::Calibration;

pub trait AbstractChannel {
    fn send(&mut self, msg: CtrlMsg);
    //fn log(&mut self, msg: &str) {}
    fn log(&mut self, _args: fmt::Arguments) {}
}

pub trait AbstractControls {
    fn set_top_power(&mut self, power: bool);
    fn set_bottom_power(&mut self, power: bool);
    fn set_fan_power(&mut self, power: bool);
    fn set_door_position(&mut self, position: i32);
    fn zero_door(&mut self);

    fn get_top_power(&self) -> bool;
    fn get_bottom_power(&self) -> bool;
    fn get_fan_power(&self) -> bool;
    fn get_door_position(&self) -> i32;
}

#[derive(Debug)]
pub struct OperationState {
    pub duration: Interval,
    pub temperature1: Temperature,
    pub temperature2: Option<Temperature>,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Completed {
    Complete,
    Incomplete,
}

#[enum_dispatch]
pub trait AbstractOperation: PartialEq {
    fn start(&mut self, controls: &mut dyn AbstractControls);

    fn run(
        &mut self,
        state: &OperationState,
        controls: &mut dyn AbstractControls,
        channel: &mut dyn AbstractChannel,
    ) -> Completed;

    fn abort(&mut self, controls: &mut dyn AbstractControls);
}

#[enum_dispatch(AbstractOperation)]
#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub enum Operation {
    JogDoor,
    BangBangControl,
    Calibration,
}
