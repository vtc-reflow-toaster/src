// TODO: it might be beneficial to periodically "unlatch" if the turn-on threshold has been met
// for a while?
use super::*;
use crate::types::logging::LogLevel;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
enum HeatState {
    Init,

    /* TODO: a preheat stage would be very helpful: warm the elements and oven up a little
     * bit to avoid the long delays during initial warm-up.
     */
    Heating {
        last_time: Interval,
        last_temp: Temperature,
        last_dtdt: TemperatureRate,
    },
    HeatingAboveLiquidus {
        last_time: Interval,
        last_temp: Temperature,
        last_dtdt: TemperatureRate,
        liquidus: Interval,
    },
    CoolingAboveLiquidus {
        cooling_began: Interval,
        start_temp: Temperature,
        liquidus: Interval,
    },
    Cooling {
        cooling_began: Interval,
        start_temp: Temperature,
    },

    Aborted,
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct BangBangControl {
    profile: TemperatureProfile,

    look_ahead: Interval,
    state: HeatState,

    top_hyst_on: TemperatureDelta,
    top_hyst_off: TemperatureDelta,
    bottom_hyst_on: TemperatureDelta,
    bottom_hyst_off: TemperatureDelta,

    door_open_position: i32,
    door_open_now: bool, // TODO move to Cooling states
    door_hyst_open: TemperatureDelta,
    door_hyst_close: TemperatureDelta,
}

impl BangBangControl {
    pub fn new(
        profile: &TemperatureProfile,
        look_ahead: Interval,
        top_hyst_on: TemperatureDelta,
        top_hyst_off: TemperatureDelta,
        bottom_hyst_on: TemperatureDelta,
        bottom_hyst_off: TemperatureDelta,
        door_open_position: i32,
        door_hyst_open: TemperatureDelta,
        door_hyst_close: TemperatureDelta,
    ) -> Self {
        Self {
            profile: profile.clone(),

            look_ahead,
            state: HeatState::Init,

            top_hyst_on,
            top_hyst_off,
            bottom_hyst_on,
            bottom_hyst_off,

            door_open_position,
            door_open_now: false,
            door_hyst_open,
            door_hyst_close,
        }
    }

    fn heat(
        &mut self,
        state: &OperationState,
        controls: &mut dyn AbstractControls,
        _channel: &mut dyn AbstractChannel,
    ) {
        let delta = state.temperature1 - self.profile.temperature_at(state.duration);

        if controls.get_top_power() {
            if delta > self.top_hyst_off {
                controls.set_top_power(false);
            }
        } else {
            if delta < self.top_hyst_on {
                controls.set_top_power(true);
            }
        }
        if controls.get_bottom_power() {
            if delta > self.bottom_hyst_off {
                controls.set_bottom_power(false);
            }
        } else {
            if delta < self.bottom_hyst_on {
                controls.set_bottom_power(true);
            }
        }
    }

    fn cool(
        &mut self,
        target: Temperature,
        state: &OperationState,
        controls: &mut dyn AbstractControls,
        _channel: &mut dyn AbstractChannel,
    ) {
        if state.temperature1 <= self.profile.cool_target {
            if self.door_open_now {
                controls.set_door_position(0);
                self.door_open_now = false;
            }
        } else {
            let delta = state.temperature1 - target;

            if self.door_open_now {
                if delta > self.door_hyst_close {
                    controls.set_door_position(0);
                    self.door_open_now = false;
                }
            } else {
                if delta < self.door_hyst_open {
                    controls.set_door_position(self.door_open_position);
                    self.door_open_now = true;
                }
            }
        }
    }
}

impl AbstractOperation for BangBangControl {
    fn start(&mut self, controls: &mut dyn AbstractControls) {
        controls.set_fan_power(true);
    }

    fn run(
        &mut self,
        state: &OperationState,
        controls: &mut dyn AbstractControls,
        channel: &mut dyn AbstractChannel,
    ) -> Completed {
        channel.send(CtrlMsg::OperationReport(
            state.duration - self.look_ahead,
            state.temperature1,
            state.temperature2,
        ));

        if self.state == HeatState::Init {
            self.state = HeatState::Heating {
                last_time: Interval::from_seconds(-1),
                last_temp: state.temperature1,
                last_dtdt: TemperatureDelta::new(0) / Interval::from_seconds(1),
            }
        }

        match self.state {
            HeatState::Init => unreachable!(),

            HeatState::Heating {
                ref mut last_time,
                ref mut last_temp,
                ref mut last_dtdt,
            }
            | HeatState::HeatingAboveLiquidus {
                ref mut last_time,
                ref mut last_temp,
                ref mut last_dtdt,
                ..
            } => {
                let dtdt = (state.temperature1 - *last_temp) / (state.duration - *last_time);
                // FIXME: make this adjustable
                let softened_dtdt = (Temperature::new(0)
                    + *last_dtdt * Interval::from_seconds(1)
                    + dtdt * Interval::from_seconds(1)
                    - Temperature::new(0))
                    / Interval::from_seconds(2);

                let pred_state = OperationState {
                    duration: state.duration + self.look_ahead,
                    temperature1: state.temperature1 + softened_dtdt * self.look_ahead,
                    temperature2: None,
                };

                *last_time = state.duration;
                *last_temp = state.temperature1;
                *last_dtdt = softened_dtdt;

                self.heat(&pred_state, controls, channel);
            }

            HeatState::Cooling {
                cooling_began,
                start_temp,
            }
            | HeatState::CoolingAboveLiquidus {
                cooling_began,
                start_temp,
                ..
            } => {
                let target = start_temp + self.profile.cool_rate * (state.duration - cooling_began);
                self.cool(target, state, controls, channel)
            }

            HeatState::Aborted => (),
        }

        match self.state {
            HeatState::Heating {
                last_time,
                last_temp,
                last_dtdt,
            } if last_temp >= self.profile.liquidus_temp => {
                channel.log(format_args!(
                    "Liquidus reached after {}s",
                    state.duration.seconds()
                ));
                self.state = HeatState::HeatingAboveLiquidus {
                    last_time,
                    last_temp,
                    last_dtdt,
                    liquidus: last_time,
                };

                Completed::Incomplete
            }

            HeatState::HeatingAboveLiquidus {
                last_time,
                last_temp,
                liquidus,
                ..
            } if (last_time - liquidus)
                + (last_temp - self.profile.liquidus_temp) / self.profile.cool_rate
                >= self.profile.time_above_liquidus =>
            {
                channel.send(CtrlMsg::Message(
                    LogLevel::Info,
                    "bang_bang",
                    "Beginning cooling",
                ));
                channel.log(format_args!(
                    "Beginning cooling after {}s",
                    state.duration.seconds()
                ));

                controls.set_top_power(false);
                controls.set_bottom_power(false);

                self.state = HeatState::CoolingAboveLiquidus {
                    cooling_began: last_time,
                    start_temp: last_temp,
                    liquidus: liquidus,
                };

                Completed::Incomplete
            }

            HeatState::CoolingAboveLiquidus {
                cooling_began,
                start_temp,
                liquidus,
            } if state.temperature1 <= self.profile.liquidus_temp => {
                channel.log(format_args!(
                    "Time above liquidus: {}s",
                    (state.duration - liquidus).seconds()
                ));

                self.state = HeatState::Cooling {
                    cooling_began,
                    start_temp,
                };

                Completed::Incomplete
            }

            HeatState::Cooling { .. } if state.temperature1 <= self.profile.cool_target => {
                controls.set_door_position(0);
                self.state = HeatState::Aborted;
                Completed::Incomplete
            }

            HeatState::Aborted if controls.get_door_position() == 0 => {
                controls.set_fan_power(false);
                Completed::Complete
            }

            _ => Completed::Incomplete,
        }
    }

    fn abort(&mut self, controls: &mut dyn AbstractControls) {
        controls.set_top_power(false);
        controls.set_bottom_power(false);
        controls.set_fan_power(false);
        controls.set_door_position(0);

        self.door_open_now = false;
        self.state = HeatState::Aborted;
    }
}
