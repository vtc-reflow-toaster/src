use super::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone, Copy)]
pub struct JogDoor(i32);

impl JogDoor {
    pub fn new(counts: i32) -> JogDoor {
        JogDoor(counts)
    }
}

impl AbstractOperation for JogDoor {
    fn start(
        &mut self,
        controls: &mut dyn AbstractControls,
    ) {
        controls.set_door_position(self.0);
    }

    fn run(
        &mut self,
        _state: &OperationState,
        controls: &mut dyn AbstractControls,
        _channel: &mut dyn AbstractChannel,
    ) -> Completed {
        if controls.get_door_position() == self.0 {
            controls.zero_door();
            Completed::Complete
        } else {
            Completed::Incomplete
        }
    }

    fn abort(
        &mut self,
        controls: &mut dyn AbstractControls,
    ) {
        controls.zero_door();
        controls.set_door_position(0);
        self.0 = 0;
    }
}
