use super::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum CalibrationStage {
    Preheat,
    PreheatSettle,
    OpenDoor,
    Reheat,
    ReheatSettle,
    StepInput,

    Abort,
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct Calibration {
    low_temp: Temperature,
    high_temp: Temperature,
    door_position: i32,
    settle_time: Interval,

    stage: CalibrationStage,
    stage_began: Interval,
}

impl Calibration {}

impl AbstractOperation for Calibration {
    fn start(
        &mut self,
        controls: &mut dyn AbstractControls,
    ) {
        controls.set_fan_power(true);
        controls.set_bottom_power(true);
        controls.set_top_power(true);
    }

    fn run(
        &mut self,
        state: &OperationState,
        controls: &mut dyn AbstractControls,
        _channel: &mut dyn AbstractChannel,
    ) -> Completed {
        match self.stage {
            CalibrationStage::Abort =>
                if controls.get_door_position() == 0 {
                    Completed::Complete
                } else {
                    Completed::Incomplete
                },

            CalibrationStage::Preheat => {
                if state.temperature1 >= self.high_temp {
                    controls.set_top_power(false);
                    controls.set_bottom_power(false);
                    self.stage = CalibrationStage::PreheatSettle;
                    self.stage_began = state.duration;
                }
                Completed::Incomplete
            }

            CalibrationStage::PreheatSettle => {
                if state.duration - self.stage_began >= self.settle_time {
                    controls.set_door_position(self.door_position);
                    self.stage = CalibrationStage::OpenDoor;
                    self.stage_began = state.duration;
                }
                Completed::Incomplete
            }

            CalibrationStage::OpenDoor => {
                // TODO: fix protocol calibration reports & send report
                if state.temperature1 <= self.low_temp {
                    controls.set_door_position(0);
                    controls.set_top_power(true);
                    controls.set_bottom_power(true);
                    self.stage = CalibrationStage::Reheat;
                    self.stage_began = state.duration;
                }
                Completed::Incomplete
            }

            CalibrationStage::Reheat => {
                if state.temperature1 >= self.low_temp {
                    controls.set_top_power(false);
                    controls.set_bottom_power(false);
                    self.stage = CalibrationStage::ReheatSettle;
                    self.stage_began = state.duration;
                }
                Completed::Incomplete
            }

            CalibrationStage::ReheatSettle => {
                if state.duration - self.stage_began >= self.settle_time {
                    controls.set_top_power(true);
                    controls.set_bottom_power(true);
                    self.stage = CalibrationStage::StepInput;
                    self.stage_began = state.duration;
                }
                Completed::Incomplete
            }

            CalibrationStage::StepInput => {
                // TODO: send report
                if state.temperature1 >= self.high_temp {
                    controls.set_top_power(false);
                    controls.set_bottom_power(false);
                    Completed::Complete
                } else {
                    Completed::Incomplete
                }
            }
        }
    }

    fn abort(
        &mut self,
        controls: &mut dyn AbstractControls,
    ) {
        self.stage = CalibrationStage::Abort;
        controls.set_top_power(false);
        controls.set_bottom_power(false);
        controls.set_fan_power(false);
        controls.set_door_position(0);
    }
}
