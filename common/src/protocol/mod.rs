use serde::{Deserialize, Serialize};
pub mod wire;
use crate::operation::calibration::CalibrationStage;
use crate::operation::Operation;
use crate::types::logging::{Error, LogLevel};
use crate::types::temperature::{Interval, Temperature};
pub use wire::WireSliceStorage;
use wire::{WireDecoder, WireEncoder};

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub enum HostMsg {
    Hello,
    Goodbye,

    RunOperation(Operation),
    AbortOperation,
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub enum CtrlMsg<'a> {
    Running,
    Stopped,

    Message(LogLevel, &'a str, &'a str),

    Temperature(Option<Temperature>),
    OperationReport(Interval, Temperature, Option<Temperature>),
    CalibrationReport(CalibrationStage, Interval, Temperature),

    InputOverflow,
    OutputOverflow,
}

pub fn encode_message<B, M>(buffer: B, msg: &M) -> Result<B, Error>
where
    B: wire::WireStorage<u8>,
    M: Serialize,
{
    let mut enc = WireEncoder::new(buffer)?;
    ssmarshal::serialize_into(&mut enc, msg)?;
    Ok(enc.finish())
}

pub fn encode_message_to_slice<'a, M>(buffer: &'a mut [u8], msg: &M) -> Result<&'a [u8], Error>
where
    M: Serialize,
{
    encode_message(wire::WireSliceStorage::new(buffer), msg).map(|wss| wss.unpack())
}

pub struct MessageDecoder<B, F>(WireDecoder<B, F>)
where
    B: wire::WireStorage<u8>,
    F: wire::WireStorage<usize>;
impl<B, F> MessageDecoder<B, F>
where
    B: wire::WireStorage<u8>,
    F: wire::WireStorage<usize>,
{
    pub fn new(buffer: B, frames: F) -> Result<MessageDecoder<B, F>, Error> {
        Ok(MessageDecoder(WireDecoder::new(buffer, frames)?))
    }

    pub fn decode_from(&mut self, bytes: impl Iterator<Item = u8>) -> Result<&mut Self, Error> {
        for byte in bytes {
            self.0.add_byte(byte)?;
        }
        Ok(self)
    }

    pub fn decode(&mut self, bytes: &[u8]) -> Result<&mut Self, Error> {
        for byte in bytes {
            self.0.add_byte(*byte)?;
        }
        Ok(self)
    }

    pub fn add_byte(&mut self, byte: u8) -> Result<&mut Self, Error> {
        self.0.add_byte(byte)?;
        Ok(self)
    }

    pub fn recv<'b, T: Deserialize<'b>>(&'b mut self) -> Result<Option<T>, Error> {
        if let Some(buf) = self.0.next_frame() {
            //println!("Next buffer: {:?}", buf);
            let framelen = buf.len();
            let (message, len) = ssmarshal::deserialize(buf)?;
            if len == framelen {
                Ok(Some(message))
            } else {
                // XXX What would this actually indicate?
                Err(Error::WireError(wire::Error::Desynchronized))
            }
        } else {
            Ok(None)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::logging::*;

    /*
    #[test]
    fn serde_works() {
        let mut buf = [0u8; 256];
        ssmarshal::serialize(&mut buf, &CtrlMsg::StepperPosition(Point::new(0, 0))).unwrap();
        let (val, _sz) = ssmarshal::deserialize::<CtrlMsg>(&buf).unwrap();
        if let CtrlMsg::StepperPosition(point) = val {
            assert_eq!(point, Point::new(0, 0));
        } else {
            panic!();
        }
    }
    */

    #[test]
    fn slices_work() {
        let strdata = "hello world";
        let mut buf = [0u8; 256];
        ssmarshal::serialize(
            &mut buf,
            &CtrlMsg::Message(LogLevel::Trace, "none", &strdata),
        )
        .unwrap();
        let (de, _sz) = ssmarshal::deserialize::<CtrlMsg>(&buf).unwrap();
        if let CtrlMsg::Message(l, t, s) = de {
            assert_eq!(l, LogLevel::Trace);
            assert_eq!(t, "none");
            assert_eq!(s, strdata);
        } else {
            panic!();
        }
    }

    #[test]
    fn roundtrip_works() {
        let a = "foo";
        let b = "bar";
        let ctrl = [
            CtrlMsg::Running,
            CtrlMsg::Stopped,
            CtrlMsg::Message(LogLevel::Debug, a, b),
            CtrlMsg::OperationReport(Interval::from_seconds(55), Temperature::new(1234)),
            //CtrlMsg::CalibrationReport
            CtrlMsg::InputOverflow,
            CtrlMsg::OutputOverflow,
        ];
        let host = [
            HostMsg::Hello,
            HostMsg::Goodbye,
            //HostMsg::RunOperation()
            HostMsg::AbortOperation,
        ];

        let mut dec = MessageDecoder::new(Vec::new(), Vec::new()).unwrap();
        /*
        for c in ctrl.iter() {
            let res = dec
                .decode(encode_message(Vec::new(), c).unwrap().as_slice())
                .unwrap()
                .recv::<CtrlMsg>()
                .unwrap();
            assert_eq!(res, Some(c.clone()));
        }
        for h in host.iter() {
            let res = dec
                .decode(encode_message(Vec::new(), h).unwrap().as_slice())
                .unwrap()
                .recv::<HostMsg>()
                .unwrap();
            assert_eq!(res, Some(h.clone()));
        }
        */
        for c in ctrl.iter() {
            dec.decode(encode_message(Vec::new(), c).unwrap().as_slice())
                .unwrap();
        }
        for h in host.iter() {
            dec.decode(encode_message(Vec::new(), h).unwrap().as_slice())
                .unwrap();
        }
        for c in ctrl.iter() {
            let res = dec.recv::<CtrlMsg>().unwrap();
            assert_eq!(res, Some(c.clone()));
        }
        for h in host.iter() {
            let res = dec.recv::<HostMsg>().unwrap();
            assert_eq!(res, Some(h.clone()));
        }
    }
}
