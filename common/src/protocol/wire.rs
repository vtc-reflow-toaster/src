/* Wire protocol:
 * Each message is encoded in a single frame.  Frames are encoded as zero-terminated COBS.  After
 * decoding from COBS, the frame contents consist of the ssmarshal-encoded message followed by a
 * checksum byte such that the data and checksum bytes sum to zero modulo 256.
 */
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Error {
    Desynchronized,
    InvalidChecksum,
    NotEnoughSpace,
    TooManyFrames,
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        let s = match *self {
            Error::Desynchronized => "Stream desynchronized",
            Error::InvalidChecksum => "Checksum mismatch",
            Error::NotEnoughSpace => "Not enough buffer space",
            Error::TooManyFrames => "Too many queued frames",
        };
        write!(f, "{}", s)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy, Default)]
pub struct Checksum(u8);

impl Checksum {
    pub fn new() -> Checksum {
        Checksum(0)
    }
    pub fn add(self, b: u8) -> Checksum {
        Checksum(self.0.wrapping_add(b))
    }
    #[allow(dead_code)]
    pub fn all<'a>(self, iter: impl Iterator<Item = &'a u8>) -> Checksum {
        iter.fold(self, |cs, b| cs.add(*b))
    }
    pub fn complement(self) -> Checksum {
        Checksum((!self.0).wrapping_add(1))
    }
    pub fn valid(self) -> bool {
        self.0 == 0
    }
}

impl Into<u8> for Checksum {
    fn into(self) -> u8 {
        self.0
    }
}

// FIXME: I hate this interface, but I can't figure out the relevant traits.
pub trait WireStorage<T> {
    fn len(&self) -> usize;
    fn free(&self) -> usize;
    fn append(&mut self, item: T) -> Result<(), ()>;
    fn drain(&mut self, count: usize);
    fn truncate(&mut self, count: usize);
    fn as_slice<'b>(&'b self) -> &'b [T];
    fn as_mut_slice<'b>(&'b mut self) -> &'b mut [T];
}

pub struct WireSliceStorage<'a, T: Copy> {
    filled: usize,
    slice: &'a mut [T],
}

impl<'a, T: Copy> WireSliceStorage<'a, T> {
    pub fn new(slice: &'a mut [T]) -> WireSliceStorage<'a, T> {
        WireSliceStorage { filled: 0, slice }
    }

    pub fn unpack(self) -> &'a [T] {
        &self.slice[..self.filled]
    }
}

impl<'a, T: Copy> WireStorage<T> for WireSliceStorage<'a, T> {
    fn len(&self) -> usize {
        self.filled
    }

    fn free(&self) -> usize {
        self.slice.len() - self.filled
    }

    fn append(&mut self, item: T) -> Result<(), ()> {
        if self.filled < self.slice.len() {
            self.slice[self.filled] = item;
            self.filled += 1;
            Ok(())
        } else {
            Err(())
        }
    }

    fn drain(&mut self, idx: usize) {
        for (si, di) in (idx..self.filled).zip(0..) {
            self.slice[di] = self.slice[si];
        }
        self.filled -= idx;
    }

    fn truncate(&mut self, count: usize) {
        self.filled = count;
    }

    fn as_slice<'b>(&'b self) -> &'b [T] {
        &self.slice[..self.filled]
    }

    fn as_mut_slice<'b>(&'b mut self) -> &'b mut [T] {
        &mut self.slice[..self.filled]
    }
}

#[cfg(feature = "std")]
impl<T: Copy> WireStorage<T> for std::vec::Vec<T> {
    // XXX do we need a resize function?
    fn len(&self) -> usize {
        std::vec::Vec::len(self)
    }

    fn free(&self) -> usize {
        usize::max_value()
    }

    fn append(&mut self, item: T) -> Result<(), ()> {
        self.push(item);
        Ok(())
    }

    fn drain(&mut self, count: usize) {
        std::vec::Vec::drain(self, ..count);
    }

    fn truncate(&mut self, count: usize) {
        std::vec::Vec::truncate(self, count);
    }

    fn as_slice<'b>(&'b self) -> &'b [T] {
        std::vec::Vec::as_slice(self)
    }

    fn as_mut_slice<'b>(&'b mut self) -> &'b mut [T] {
        std::vec::Vec::as_mut_slice(self)
    }
}

pub struct WireEncoder<T: WireStorage<u8>> {
    buffer: T,
    length_idx: usize,
    checksum: Checksum,
}

impl<T: WireStorage<u8>> WireEncoder<T> {
    pub fn new(mut buffer: T) -> Result<WireEncoder<T>, Error> {
        /* The encoder requires three bytes of space to emit an empty message: length, checksum,
         * terminating zero.
         */
        if buffer.free() >= 3 {
            // Reserve a length byte now
            buffer.append(0).map_err(|_| Error::NotEnoughSpace)?;

            Ok(WireEncoder {
                buffer,
                length_idx: 0,
                checksum: Checksum::new(),
            })
        } else {
            Err(Error::NotEnoughSpace)
        }
    }

    fn complete_block(&mut self) {
        /* Update the length byte for this block, then set aside the current byte for the length of
         * the next block.
         */
        self.buffer.as_mut_slice()[self.length_idx] = (self.buffer.len() - self.length_idx) as u8;

        // Carve out a length byte for the next block
        self.length_idx = self.buffer.len();
        self.buffer.append(0).unwrap();
    }

    fn add_byte_internal(&mut self, byte: u8) {
        if byte == 0 {
            // Zero byte does not appear in output; start a new block
            self.complete_block();
        } else {
            // Non-zero byte appears as-is in the output, but long runs may need multiple blocks
            if self.buffer.len() - self.length_idx == 255 {
                self.complete_block()
            }
            self.buffer.append(byte).unwrap();
        }
    }

    pub fn add_byte(&mut self, byte: u8) -> Result<(), Error> {
        // Ensure that extra bytes are always free for length byte, checksum, and terminating zero.
        if self.buffer.free() > 3 {
            self.checksum = self.checksum.add(byte);
            self.add_byte_internal(byte);
            Ok(())
        } else {
            Err(Error::NotEnoughSpace)
        }
    }

    pub fn finish(mut self) -> T {
        // We've guaranteed ourselves three extra bytes at the end of the buffer:
        // 1. Append our checksum to the end of the frame.
        self.add_byte_internal(self.checksum.complement().into());
        // 2. An implicit zero follows the data; we must always complete a block at the end.
        // 3. complete_block() also inserts a terminating zero byte.
        self.complete_block();

        self.buffer
    }
}

impl<T: WireStorage<u8>> ssmarshal::ByteSink for WireEncoder<T> {
    fn put_byte(&mut self, byte: u8) -> Result<(), ()> {
        self.add_byte(byte).map_err(|_| ())
    }

    fn put_bytes(&mut self, bytes: &[u8]) -> Result<(), ()> {
        if self.buffer.free() > 3 {
            for byte in bytes {
                self.put_byte(*byte)?;
            }
            Ok(())
        } else {
            Err(())
        }
    }
}

/* COBS decoder: accept data one byte at a time, decoding into a flat buffer and counting zero
 * bytes.
 */
pub struct WireDecoder<B, F>
where
    B: WireStorage<u8>,
    F: WireStorage<usize>,
{
    buffer: B,
    frame_idxes: F,
    // Beginning of frame currently being received, stashed to drop corrupt frames
    last_begin: usize,
    // Remaining bytes in this block
    block_rem: usize,
    // Whether this block ends in a zero byte
    block_zero: bool,
    // Checksum of current frame
    checksum: Checksum,
}

impl<B, F> WireDecoder<B, F>
where
    B: WireStorage<u8>,
    F: WireStorage<usize>,
{
    pub fn new(buffer: B, frame_idxes: F) -> Result<WireDecoder<B, F>, Error> {
        // The decoder has two bytes of buffer overhead for checksum and implicit zero byte.
        if buffer.free() >= 2 && frame_idxes.free() > 0 {
            Ok(WireDecoder {
                buffer,
                frame_idxes,
                last_begin: 0,
                block_rem: 0,
                block_zero: false,
                checksum: Checksum::new(),
            })
        } else {
            Err(Error::NotEnoughSpace)
        }
    }

    // Mark this frame invalid: a properly coded frame will end at the end of a block
    fn desync(&mut self) {
        self.block_rem = usize::max_value();
    }

    fn write_byte(&mut self, byte: u8) -> Result<(), Error> {
        self.buffer.append(byte).map_err(|_| Error::NotEnoughSpace)
    }

    fn discard_frame(&mut self) {
        self.buffer.truncate(self.last_begin);
        self.checksum = Checksum::new();
        self.block_rem = 0;
    }

    fn accept_frame(&mut self) -> Result<(), Error> {
        match self.frame_idxes.append(self.last_begin) {
            Ok(_) => {
                self.last_begin = self.buffer.len();
                self.block_rem = 0;
                self.checksum = Checksum::new();
                Ok(())
            }
            Err(_) => {
                self.discard_frame();
                Err(Error::TooManyFrames)
            }
        }
    }

    fn zero_byte(&mut self) -> Result<(), Error> {
        // Overwrite the checksum byte and the implicit zero at the end of the data.
        // XXX what would cause this to underflow?
        self.buffer.truncate(self.last_begin.max(self.buffer.len().max(2) - 2));

        // Verify the checksum and save or discard the frame
        if self.block_rem != 0 {
            self.discard_frame();
            Err(Error::Desynchronized)
        } else if self.checksum.valid() {
            self.accept_frame()
        } else {
            self.discard_frame();
            Err(Error::InvalidChecksum)
        }
    }

    fn nonzero_byte(&mut self, byte: u8) -> Result<(), Error> {
        if self.block_rem == 0 {
            // This byte is the length of a block of non-zero bytes
            self.block_zero = byte != 255;
            self.block_rem = usize::from(byte - 1);
        } else {
            // This byte is a literal non-zero byte
            self.checksum = self.checksum.add(byte);
            self.write_byte(byte)?;
            self.block_rem -= 1;
        }

        // Emit a zero byte at the end of the block
        if self.block_rem == 0 && self.block_zero {
            self.write_byte(0)?;
        }

        Ok(())
    }

    pub fn add_byte(&mut self, byte: u8) -> Result<(), Error> {
        if byte == 0 {
            self.zero_byte()
        } else if let Err(e) = self.nonzero_byte(byte) {
            self.discard_frame();
            self.desync();
            Err(e)
        } else {
            Ok(())
        }
    }
}

impl<B, F> WireDecoder<B, F>
where
    B: WireStorage<u8>,
    F: WireStorage<usize>,
{
    // Get the length of the current frame, before it is shifted out.
    fn frame_length(&self, i: usize) -> usize {
        self.frame_idxes
            .as_slice()
            .get(i)
            .map_or(self.last_begin, |&i| i)
    }

    fn shift_bytes(&mut self) {
        /* next_frame() shifted out the bottom index, so the next frame's index is in the bottom
         * index.
         */
        let end = self.frame_length(0);
        if end != 0 {
            self.buffer.drain(end);
            self.last_begin -= end;
            for v in self.frame_idxes.as_mut_slice().iter_mut() {
                *v -= end;
            }
        }
    }

    pub fn next_frame(&mut self) -> Option<&[u8]> {
        self.shift_bytes();

        if self.frame_idxes.len() != 0 {
            let end = self.frame_length(1);
            // We know that this slice starts at 0, so we can drop the index early.
            self.frame_idxes.drain(1);
            Some(&self.buffer.as_slice()[0..end])
        } else {
            None
        }
    }
}

#[cfg(all(test, feature = "std"))]
mod test {
    use super::*;

    #[test]
    fn no_zeros() {
        let data: [u8; 10] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0];
        let mut enc_buf = [0u8; 20];
        let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
        for b in &data {
            enc.add_byte(*b).unwrap();
        }

        let s = enc.finish();
        for b in &s.as_slice()[..s.len() - 1] {
            assert_ne!(*b, 0);
        }
        assert_eq!(s.as_slice()[s.len() - 1], 0);
    }

    #[test]
    fn cobs_round_trip() {
        let data: [u8; 10] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0];
        let mut enc_buf = [0u8; 20];
        let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
        for b in &data {
            enc.add_byte(*b).unwrap();
        }

        let s = enc.finish();
        let mut dec_buf = [0u8; 20];
        let mut dec_idx = [0usize; 4];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for b in s.as_slice() {
            dec.add_byte(*b).unwrap();
        }
        if let Some(d) = dec.next_frame() {
            assert_eq!(d.len(), data.len());
            for (a, b) in d.iter().zip(data.iter()) {
                assert_eq!(*a, *b);
            }
        } else {
            panic!();
        }
    }

    #[test]
    fn long_round_trip() {
        let data = [1u8; 512];
        let mut enc_buf = [0u8; 1024];
        let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
        for b in data.iter() {
            enc.add_byte(*b).unwrap();
        }

        let s = enc.finish();
        let mut dec_buf = [0u8; 1024];
        let mut dec_idx = [0usize; 4];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for b in s.as_slice() {
            dec.add_byte(*b).unwrap();
        }
        if let Some(d) = dec.next_frame() {
            assert_eq!(d.len(), data.len());
            for (a, b) in d.iter().zip(data.iter()) {
                assert_eq!(*a, *b);
            }
        } else {
            panic!();
        }
    }

    #[test]
    fn almost_254() {
        let mut data = [1u8; 512];
        data[253] = 0;
        data[253 + 254] = 0;
        let mut enc_buf = [0u8; 1024];
        let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
        for b in data.iter() {
            enc.add_byte(*b).unwrap();
        }

        let s = enc.finish();
        let mut dec_buf = [0u8; 1024];
        let mut dec_idx = [0usize; 4];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for b in s.as_slice() {
            dec.add_byte(*b).unwrap();
        }
        if let Some(d) = dec.next_frame() {
            assert_eq!(d.len(), data.len());
            for (a, b) in d.iter().zip(data.iter()) {
                assert_eq!(*a, *b);
            }
        } else {
            panic!();
        }
    }

    #[test]
    fn many_frames() {
        let data: [u8; 10] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0];
        let mut enc_buf = [0u8; 1024];
        let mut dest: Vec<u8> = Vec::new();
        for _ in 0..5 {
            let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
            for b in &data {
                enc.add_byte(*b).unwrap();
            }
            dest.extend(enc.finish().as_slice())
        }

        let mut dec_buf = [0u8; 1024];
        let mut dec_idx = [0usize; 10];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for b in dest.iter() {
            dec.add_byte(*b).unwrap();
        }
        for b in dest.iter() {
            dec.add_byte(*b).unwrap();
        }
        /* FIXME: currently, we leave the contents of the last frame sitting in the buffer.  It
         * might be better to return a Drop reference that cleans up the bytes.
         */
        for _ in 0..6 {
            let d = dec.next_frame().unwrap();
            assert_eq!(d.len(), data.len());
            for (a, b) in d.iter().zip(data.iter()) {
                assert_eq!(*a, *b);
            }
        }

        for b in dest.iter() {
            dec.add_byte(*b).unwrap();
        }

        for _ in 0..9 {
            let d = dec.next_frame().unwrap();
            assert_eq!(d.len(), data.len());
            for (a, b) in d.iter().zip(data.iter()) {
                assert_eq!(*a, *b);
            }
        }
    }

    #[test]
    fn corruption() {
        for i in 0..12 {
            let data: [u8; 10] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0];
            let mut enc_buf = [0u8; 20];
            let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
            for b in &data {
                enc.add_byte(*b).unwrap();
            }

            let mut sum_failed = false;
            let mut desync = false;
            let s_imm = enc.finish();
            let mut s = s_imm.as_slice().to_owned();
            s[i] += 1;
            let mut dec_buf = [0u8; 20];
            let mut dec_idx = [0usize; 2];
            let mut dec = WireDecoder::new(
                WireSliceStorage::new(&mut dec_buf),
                WireSliceStorage::new(&mut dec_idx),
            )
            .unwrap();
            for b in &s {
                match dec.add_byte(*b) {
                    Err(Error::InvalidChecksum) => {
                        sum_failed = true;
                    }
                    Err(Error::Desynchronized) => {
                        desync = true;
                    }
                    _ => {}
                }
            }
            assert!(dec.next_frame().is_none());
            assert!(sum_failed || desync);
        }
    }

    #[test]
    fn overflow() {
        let data: [u8; 10] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0];
        let mut enc_buf = [0u8; 1024];
        let mut dest: Vec<u8> = Vec::new();
        for _ in 0..2 {
            let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
            for b in &data {
                enc.add_byte(*b).unwrap();
            }
            dest.extend(enc.finish().as_slice())
        }

        let mut insert_failed = false;
        let mut dec_buf = [0u8; 20];
        let mut dec_idx = [0usize; 1];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for b in dest.iter() {
            insert_failed |= dec.add_byte(*b).is_err();
        }
        assert!(insert_failed);
        assert!(dec.next_frame().is_some());
        assert!(dec.next_frame().is_none());
    }

    #[test]
    fn overflow_errors() {
        let mut dec_buf = [0u8; 10];
        let mut dec_idx = [0usize; 1];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for b in (12..).take(11) {
            dec.add_byte(b).unwrap();
        }
        assert!(dec.add_byte(55).is_err());
    }

    #[test]
    fn empty_outputs() {
        let input = [1u8, 1, 0, 1, 1, 0];
        let mut dec_buf = [0u8; 10];
        let mut dec_idx = [0usize; 2];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for &b in input.iter() {
            dec.add_byte(b).unwrap();
        }
        assert!(dec.next_frame().is_some());
        assert!(dec.next_frame().is_some());
    }

    #[test]
    fn small_frame_idxes() {
        let input = [3u8, 5, 251, 0];
        let mut dec_buf = [0u8; 10];
        let mut dec_idx = [0usize; 1];
        let mut dec = WireDecoder::new(
            WireSliceStorage::new(&mut dec_buf),
            WireSliceStorage::new(&mut dec_idx),
        )
        .unwrap();
        for &b in input.iter() {
            dec.add_byte(b).unwrap();
        }
        assert!(dec.next_frame().is_some());
        //assert!(dec.next_frame().is_none());
        for &b in input.iter() {
            dec.add_byte(b).unwrap();
        }
        assert!(dec.next_frame().is_some());
        //assert!(dec.next_frame().is_none());
    }

    #[test]
    fn small_enc_buffer() {
        let data: [u8; 10] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0];
        /* XXX 4 bytes overhead; only 3 are strictly required.  It might be worth checking why this
         * doesn't work for 4 bytes overhead; it's probably a bug.
         */
        let mut enc_buf = [0u8; 14];
        let mut enc = WireEncoder::new(WireSliceStorage::new(&mut enc_buf)).unwrap();
        for b in &data {
            enc.add_byte(*b).unwrap();
        }

        let s = enc.finish();
        for b in &s.as_slice()[..s.len() - 1] {
            assert_ne!(*b, 0);
        }
        assert_eq!(s.as_slice()[s.len() - 1], 0);
    }
}
