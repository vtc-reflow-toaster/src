/// Temperature: fixed-point calculations for temperatures and related units
use core::ops::{Add, Div, Mul, Sub};
use serde::{Deserialize, Serialize};

fn div_round(num: i32, den: i32) -> i32 {
    (num + den / 2) / den
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Interval(i32);
impl Interval {
    pub const SCALE: i32 = 1000;
    pub fn from_seconds(sec: i32) -> Interval {
        Interval(sec * Interval::SCALE)
    }
    pub fn from_millis(msec: i32) -> Interval {
        Interval(msec)
    }
    pub fn seconds(self) -> i32 {
        div_round(self.0, Interval::SCALE)
    }
    pub fn millis(self) -> i32 {
        self.0
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Temperature(i32);
impl Temperature {
    pub const SCALE: i32 = 1 << 20;

    pub fn new(deg: i32) -> Temperature {
        Temperature(deg * Temperature::SCALE)
    }
    pub fn new_prescaled(val: i32) -> Temperature {
        Temperature(val)
    }
    pub fn degrees(self) -> i32 {
        div_round(self.0, Temperature::SCALE)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct TemperatureDelta(i32);
impl TemperatureDelta {
    pub const SCALE: i32 = Temperature::SCALE;

    pub fn new(deg: i32) -> TemperatureDelta {
        TemperatureDelta(deg * TemperatureDelta::SCALE)
    }
    pub fn new_prescaled(val: i32) -> TemperatureDelta {
        TemperatureDelta(val)
    }
    pub fn degrees(self) -> i32 {
        div_round(self.0, TemperatureDelta::SCALE)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct TemperatureRate(i32);

#[cfg(feature = "std")]
impl From<f32> for Interval {
    fn from(t: f32) -> Interval {
        Interval((t * 1000.0).round() as i32)
    }
}
#[cfg(feature = "std")]
impl Into<f32> for Interval {
    fn into(self) -> f32 {
        self.0 as f32 / 1000.0
    }
}
#[cfg(feature = "std")]
impl From<f64> for Interval {
    fn from(t: f64) -> Interval {
        Interval((t * 1000.0).round() as i32)
    }
}
#[cfg(feature = "std")]
impl Into<f64> for Interval {
    fn into(self) -> f64 {
        self.0 as f64 / 1000.0
    }
}

#[cfg(feature = "std")]
impl From<f32> for Temperature {
    fn from(t: f32) -> Temperature {
        Temperature((t * Temperature::SCALE as f32).round() as i32)
    }
}
#[cfg(feature = "std")]
impl Into<f32> for Temperature {
    fn into(self) -> f32 {
        self.0 as f32 / Temperature::SCALE as f32
    }
}
#[cfg(feature = "std")]
impl From<f64> for Temperature {
    fn from(t: f64) -> Temperature {
        Temperature((t * Temperature::SCALE as f64).round() as i32)
    }
}
#[cfg(feature = "std")]
impl Into<f64> for Temperature {
    fn into(self) -> f64 {
        self.0 as f64 / Temperature::SCALE as f64
    }
}

#[cfg(feature = "std")]
impl From<f32> for TemperatureDelta {
    fn from(t: f32) -> TemperatureDelta {
        TemperatureDelta((t * TemperatureDelta::SCALE as f32).round() as i32)
    }
}
#[cfg(feature = "std")]
impl Into<f32> for TemperatureDelta {
    fn into(self) -> f32 {
        self.0 as f32 / TemperatureDelta::SCALE as f32
    }
}
#[cfg(feature = "std")]
impl From<f64> for TemperatureDelta {
    fn from(t: f64) -> TemperatureDelta {
        TemperatureDelta((t * TemperatureDelta::SCALE as f64).round() as i32)
    }
}
#[cfg(feature = "std")]
impl Into<f64> for TemperatureDelta {
    fn into(self) -> f64 {
        self.0 as f64 / TemperatureDelta::SCALE as f64
    }
}

#[cfg(feature = "std")]
impl From<f32> for TemperatureRate {
    fn from(t: f32) -> TemperatureRate {
        TemperatureDelta::from(t) / Interval::from_seconds(1)
    }
}
#[cfg(feature = "std")]
impl Into<f32> for TemperatureRate {
    fn into(self) -> f32 {
        (self * Interval::from_seconds(1)).into()
    }
}
#[cfg(feature = "std")]
impl From<f64> for TemperatureRate {
    fn from(t: f64) -> TemperatureRate {
        TemperatureDelta::from(t) / Interval::from_seconds(1)
    }
}
#[cfg(feature = "std")]
impl Into<f64> for TemperatureRate {
    fn into(self) -> f64 {
        (self * Interval::from_seconds(1)).into()
    }
}

impl Add for Interval {
    type Output = Interval;

    fn add(self, rhs: Interval) -> Interval {
        Interval(self.0 + rhs.0)
    }
}

impl Sub for Interval {
    type Output = Interval;

    fn sub(self, rhs: Interval) -> Interval {
        Interval(self.0 - rhs.0)
    }
}

impl Add<TemperatureDelta> for Temperature {
    type Output = Temperature;

    fn add(self, rhs: TemperatureDelta) -> Temperature {
        Temperature(self.0 + rhs.0)
    }
}

impl Sub<TemperatureDelta> for Temperature {
    type Output = Temperature;

    fn sub(self, rhs: TemperatureDelta) -> Temperature {
        Temperature(self.0 - rhs.0)
    }
}

impl Sub for Temperature {
    type Output = TemperatureDelta;

    fn sub(self, rhs: Temperature) -> TemperatureDelta {
        TemperatureDelta(self.0 - rhs.0)
    }
}

impl Div<Interval> for TemperatureDelta {
    type Output = TemperatureRate;

    fn div(self, rhs: Interval) -> TemperatureRate {
        TemperatureRate(div_round(self.0, rhs.0))
    }
}

impl Mul<Interval> for TemperatureRate {
    type Output = TemperatureDelta;

    fn mul(self, rhs: Interval) -> TemperatureDelta {
        TemperatureDelta(self.0 * rhs.0)
    }
}

impl Div<TemperatureRate> for TemperatureDelta {
    type Output = Interval;

    fn div(self, rhs: TemperatureRate) -> Interval {
        Interval(div_round(self.0, rhs.0))
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
#[serde(default)]
pub struct TemperatureProfile {
    pub soak_ramp_rate: TemperatureRate,
    pub soak_ramp_target: Temperature,

    pub soak_time: Interval,
    pub soak_target: Temperature,

    pub reflow_ramp_rate: TemperatureRate,
    pub liquidus_temp: Temperature,
    pub time_above_liquidus: Interval,
    pub temp_limit: Temperature,

    pub cool_rate: TemperatureRate, // NB: positive
    pub cool_target: Temperature,
}

impl TemperatureProfile {
    fn soak_ramp_end(&self) -> Interval {
        (self.soak_ramp_target - Temperature::new(25)) / self.soak_ramp_rate
    }

    #[cfg(feature = "std")]
    pub fn point_vec(&self) -> Vec<(Interval, Temperature)> {
        let mut ret = Vec::new();

        let soak_ramp_end = self.soak_ramp_end();
        let reflow_time_to_liquidus: Interval =
            (self.liquidus_temp - self.soak_target) / self.reflow_ramp_rate;
        let rate_sum = TemperatureRate(self.cool_rate.0 + self.reflow_ramp_rate.0);
        let reflow_time_to_peak: Interval = (self.cool_rate * self.time_above_liquidus) / rate_sum;
        let reflow_peak_time = reflow_time_to_liquidus + reflow_time_to_peak;
        // TODO: show limited peak temp
        let reflow_peak_temp = self.soak_target + self.reflow_ramp_rate * reflow_peak_time;
        let cool_time = (reflow_peak_temp - self.cool_target) / self.cool_rate;

        ret.push((Interval::from_seconds(0), Temperature::new(25)));
        ret.push((soak_ramp_end, self.soak_ramp_target));
        ret.push((soak_ramp_end + self.soak_time, self.soak_target));
        if reflow_peak_temp < self.temp_limit {
            ret.push((
                soak_ramp_end + self.soak_time + reflow_peak_time,
                reflow_peak_temp,
            ));
        } else {
            let time_to_limit = (self.temp_limit - self.soak_target) / self.reflow_ramp_rate;
            let time_to_cool_to_limit = (reflow_peak_temp - self.temp_limit) / self.cool_rate;
            ret.push((
                soak_ramp_end + self.soak_time + time_to_limit,
                self.temp_limit,
            ));
            ret.push((
                soak_ramp_end + self.soak_time + reflow_peak_time + time_to_cool_to_limit,
                self.temp_limit,
            ));
        }
        ret.push((
            soak_ramp_end + self.soak_time + reflow_peak_time + cool_time,
            self.cool_target,
        ));

        ret
    }

    pub fn temperature_at(&self, time: Interval) -> Temperature {
        let soak_ramp_end = self.soak_ramp_end();

        let temp = if time <= soak_ramp_end {
            Temperature::new(25) + self.soak_ramp_rate * time
        } else if time <= soak_ramp_end + self.soak_time {
            self.soak_ramp_target
                + (self.soak_target - self.soak_ramp_target) / self.soak_time
                    * (time - soak_ramp_end)
        } else {
            // NB cooling phase not implemented
            self.soak_target + self.reflow_ramp_rate * (time - soak_ramp_end - self.soak_time)
        };

        if temp >= self.temp_limit {
            self.temp_limit
        } else {
            temp
        }
    }
}

impl Default for TemperatureProfile {
    fn default() -> TemperatureProfile {
        let room_temp = Temperature::new(25);
        let soak_ramp_target = Temperature::new(150);
        let soak_ramp_time = Interval::from_seconds(90);
        let soak_ramp_rate = (soak_ramp_target - room_temp) / soak_ramp_time;

        let soak_time = Interval::from_seconds(90);
        let soak_target = Temperature::new(175);

        let liquidus_temp = Temperature::new(217);
        let time_to_liquidus = Interval::from_seconds(30);
        let reflow_ramp_rate = (liquidus_temp - soak_target) / time_to_liquidus;
        let time_above_liquidus = Interval::from_seconds(60);
        let temp_limit = Temperature::new(249);

        let cool_rate = (Temperature::new(3) - Temperature::new(0)) / Interval::from_seconds(1);
        let cool_target = Temperature::new(30);

        TemperatureProfile {
            soak_ramp_rate,
            soak_ramp_target,

            soak_time,
            soak_target,

            reflow_ramp_rate,
            liquidus_temp,
            time_above_liquidus,
            temp_limit: temp_limit,

            cool_rate,
            cool_target,
        }
    }
}
