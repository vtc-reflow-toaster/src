use crate::protocol::wire;
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub enum Error {
    MarshalError(ssmarshal::Error),
    WireError(wire::Error),
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match *self {
            Error::MarshalError(ref e) => write!(f, "ssmarshal error: {}", e),
            Error::WireError(ref e) => write!(f, "wire error: {}", e),
        }
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}

impl From<ssmarshal::Error> for Error {
    fn from(err: ssmarshal::Error) -> Error {
        Error::MarshalError(err)
    }
}

impl From<wire::Error> for Error {
    fn from(err: wire::Error) -> Error {
        Error::WireError(err)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum LogLevel {
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}
#[cfg(feature = "log-interop")]
impl LogLevel {
    pub fn as_log_level(self) -> log::Level {
        match self {
            LogLevel::Error => log::Level::Error,
            LogLevel::Warn => log::Level::Warn,
            LogLevel::Info => log::Level::Info,
            LogLevel::Debug => log::Level::Debug,
            LogLevel::Trace => log::Level::Trace,
        }
    }
}
