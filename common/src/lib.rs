#![cfg_attr(not(any(test, feature = "std")), no_std)]

pub mod protocol;
pub mod types;
pub mod operation;
pub extern crate arr_macro;

#[macro_export]
macro_rules! declare_ring_buffer {
    ($name:ident, $len:expr) => {
        pub struct $name<T>(RBIndex, RBIndex, [core::mem::ManuallyDrop<T>; $len]);
        impl<T> $name<T> {
            pub const fn new(init: [core::mem::ManuallyDrop<T>; $len]) -> $name<T> {
                $name(0, 0, init)
            }
        }
        impl<T> Storage for $name<T> {
            type Item = T;

            fn indices(&self) -> (RBIndex, RBIndex, RBIndex) {
                (self.0, self.1, self.2.len() as RBIndex)
            }
            fn indices_mut(&mut self) -> (&mut RBIndex, &mut RBIndex, RBIndex) {
                (&mut self.0, &mut self.1, self.2.len() as RBIndex)
            }
            fn get(&mut self, idx: RBIndex) -> T {
                unsafe { core::mem::replace(&mut self.2[idx as usize], core::mem::uninitialized()) }
            }
            fn set(&mut self, idx: RBIndex, val: T) {
                self.2[idx as usize] = core::mem::ManuallyDrop::new(val);
            }
        }
        impl<T> RingBuffer for $name<T> {}
        impl<T: Clone> RingBufferExtend<T> for $name<T> {}
    };
}

// FIXME: allow paths in $type
#[macro_export]
macro_rules! define_ring_buffer {
    ($type:ident : [ $init:expr ; $len:expr ]) => {
        $type::new($crate::arr_macro::arr![core::mem::ManuallyDrop::new($init); $len])
    }
}

pub mod ring_buffer;
