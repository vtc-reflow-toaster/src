#[path = "../src/ui/graph.rs"]
mod graph;

use graph::Model as _;
use gtk::prelude::*;
use gtk::Orientation::{Horizontal, Vertical};
use relm::{connect, connect_stream, Relm, Update, Widget};
use relm_derive::Msg;

#[derive(Msg, Debug)]
pub enum Msg {
    Quit,
    RedrawGraph,
    ChangeAxes,
    Randomize,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
enum Series {
    First,
    Second,
}

pub struct Model {
    graph: graph::View,
    gmdl: graph::VecModel<Series>,
}

impl Model {
    fn random_series(
        &mut self,
        series: Series,
        xrange: &graph::Scale,
        yrange: &graph::Scale,
        count: usize,
    ) {
        self.gmdl.remove_series(series);
        let s = self.gmdl.points_mut(series);
        for _ in 0..count {
            s.push(graph::Point(
                rand::random::<f64>() * (xrange.end - xrange.start) + xrange.start,
                rand::random::<f64>() * (yrange.end - yrange.start) + yrange.start,
            ));
        }
    }
}

#[allow(dead_code)]
struct Win {
    model: Model,

    win: gtk::Window,
    win_box: gtk::Box,
    graph_frame: gtk::Frame,
    graph_area: gtk::DrawingArea,
    control_frame: gtk::Frame,
    control_box: gtk::Box,
    x_scale: gtk::SpinButton,
    y_scale: gtk::SpinButton,
    secondary: gtk::CheckButton,
    symmetric: gtk::CheckButton,
    randomize: gtk::Button,
}

impl Update for Win {
    type Model = Model;
    type ModelParam = ();
    type Msg = Msg;

    fn model(_: &Relm<Self>, _: ()) -> Model {
        Model {
            graph: graph::View::new(),
            gmdl: graph::VecModel::new(),
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Quit => {
                gtk::main_quit();
            }

            Msg::RedrawGraph => {
                let gmdl = &mut self.model.gmdl;
                self.model.graph.draw(gmdl, &mut self.graph_area);
                gmdl.did_redraw();
            }

            Msg::ChangeAxes => {
                let upper = (self.x_scale.get_value(), self.y_scale.get_value());
                let lower = if self.symmetric.get_active() {
                    (-self.x_scale.get_value(), -self.y_scale.get_value())
                } else {
                    (0.0, 0.0)
                };

                let axes = self.model.gmdl.axes_mut();
                axes.0.scale = lower.0..upper.0;
                axes.1.scale = lower.1..upper.1;

                self.graph_area.queue_draw();
            }

            Msg::Randomize => {
                let axes = self.model.gmdl.axes_mut();
                let xscale = axes.0.scale.clone();
                let yscale = axes.1.scale.clone();

                self.model
                    .random_series(Series::First, &xscale, &yscale, 10);
                self.model.gmdl.path_mut(Series::First).color[2] = 1.0;

                if self.secondary.get_active() {
                    self.model
                        .random_series(Series::Second, &xscale, &yscale, 10);
                    self.model.gmdl.path_mut(Series::Second).color[0] = 1.0;
                }

                self.graph_area.queue_draw();
            }
        }
    }
}

impl Widget for Win {
    type Root = gtk::Window;

    fn root(&self) -> Self::Root {
        self.win.clone()
    }

    fn view(relm: &Relm<Self>, mut model: Self::Model) -> Self {
        let win = gtk::Window::new(gtk::WindowType::Toplevel);
        win.set_title("graph.rs demo");
        connect!(
            relm,
            win,
            connect_delete_event(_, _),
            return (Msg::Quit, Inhibit(false))
        );

        let win_box = gtk::Box::new(Vertical, 5);
        win.add(&win_box);

        let graph_frame = gtk::Frame::new(Some("Graph"));
        graph_frame.set_vexpand(true);
        win_box.add(&graph_frame);

        let graph_area = gtk::DrawingArea::new();
        connect!(
            relm,
            graph_area,
            connect_draw(_, _),
            return (Msg::RedrawGraph, Inhibit(false))
        );
        graph_frame.add(&graph_area);

        let control_frame = gtk::Frame::new(Some("Controls"));
        win_box.add(&control_frame);

        let control_box = gtk::Box::new(Horizontal, 5);
        control_frame.add(&control_box);

        let x_scale = gtk::SpinButton::new_with_range(10.0, 10000.0, 10.0);
        connect!(
            relm,
            x_scale,
            connect_property_value_notify(_),
            Msg::ChangeAxes
        );
        control_box.add(&x_scale);

        let y_scale = gtk::SpinButton::new_with_range(10.0, 10000.0, 10.0);
        connect!(
            relm,
            y_scale,
            connect_property_value_notify(_),
            Msg::ChangeAxes
        );
        control_box.add(&y_scale);

        let secondary = gtk::CheckButton::new();
        control_box.add(&secondary);

        let symmetric = gtk::CheckButton::new();
        connect!(relm, symmetric, connect_clicked(_), Msg::ChangeAxes);
        control_box.add(&symmetric);

        let randomize = gtk::Button::new_with_label("Randomize");
        connect!(relm, randomize, connect_clicked(_), Msg::Randomize);
        control_box.add(&randomize);

        win.show_all();

        model.graph.init(&graph_area);
        let axes = &mut model.gmdl.axes_mut();
        axes.0.scale = 0.0..x_scale.get_value();
        axes.1.scale = 0.0..y_scale.get_value();

        Win {
            model,

            win,
            win_box,
            graph_frame,
            graph_area,
            control_frame,
            control_box,
            x_scale,
            y_scale,
            secondary,
            symmetric,
            randomize,
        }
    }
}

pub fn main() {
    Win::run(()).unwrap();
}
