pub mod thread;
mod ui;
mod uart;

use crate::ui::Msg as UIMsg;

enum Msg {
    Exit,

    UARTEnumerate,
    UARTConnect(String),
    UARTDisconnect,

    StateChanged,
}
