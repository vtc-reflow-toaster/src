use super::{ui::UI, uart::UART};
use failure::{err_msg, Fallible, ResultExt};
use mio::{Poll, PollOpt, Ready, Token};
use mio_extras::channel::Receiver;
use std::io::{self, prelude::*};

pub fn uart_thread(/* XXX what? */) {
    trace!("Entered UART thread");

    let mut state = Thread::new(ui);
    state.run();

    trace!("Shutting down");
}

struct Thread {
    ui: UI,
    poll: Poll,
    uart: Option<UART>,
}

const UI_TOKEN: Token = Token(0);
const UART_TOKEN: Token = Token(1);

impl Thread {
    fn new(ui: UI) -> Thread {
        let poll = Poll::new()
            .map_err(|e| error!("Unable to create poll instance: {:?}", e))
            .unwrap();

        Thread {
            ui,
            poll,
            uart: None,
        }
    }

    fn run(&mut self) {
        self.poll
            .register(
                &self.ui.uart_events,
                UI_TOKEN,
                Ready::readable(),
                PollOpt::level(),
            )
            .map_err(|e| error!("Unable to attach UI to poll instance: {:?}", e))
            .unwrap();

        let mut events = Events::with_capacity(32);
        trace!("Entering UART poll loop");
        'main: loop {
            self.poll
                .poll(&mut events, None)
                .map_err(|e| error!("Unable to poll: {:?}", e))
                .unwrap();

            for event in events.iter() {
                match event.token() {
                    UI_TOKEN => {
                        if self.ui.recv().and_then(|msg| self.handle_ui(msg)).is_err() {
                            break 'main;
                        }
                    }

                    UART_TOKEN => {
                        let uart = self.uart.as_mut().unwrap();
                        let (disconnect, err) = uart.ready(event.readiness().into());

                        if let Err(e) = err {
                            report!(e.context("Error polling UART"));
                        }

                        if let Err(e) = uart.recv_into(&mut self.ui) {
                            report!(e.context("Error handling received messages"));
                        }

                        if disconnect {
                            self.disconnect();
                        }
                    }

                    _ => {
                        report!(err_msg("Unknown event token received"));
                        panic!();
                    }
                };
            }
        }
    }
}
