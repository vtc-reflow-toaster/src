use std::sync::{Arc, Mutex};
use crate::state;
use super::{Msg as WorkerMsg, UIMsg};
use relm::Sender;
use mio_extras::channel::Receiver;

pub struct UI {
    pub sender: Sender<UIMsg>,
    pub reveiver: Receiver<WorkerMsg>,
}

impl UI {
    pub fn new(sender: Sender<UIMsg>, reveiver: Receiver<WorkerMsg>) -> UI {
        UI {
            sender,
            reveiver,
        }
    }

    pub fn send(&mut self, msg: UIMsg) -> Fallible<()> {
        self.sender
            .send(msg)
            .context("Error sending message to UI thread")?;
        Ok(())
    }

    pub fn recv(&mut self) -> Fallible<Msg> {
        Ok(self
            .reveiver
            .try_recv()
            .context("Error receiving message from UI thread")?)
    }
}
