use std::sync::{Arc, Mutex};
use crate::state;
use super::{UIMsg, Msg as WorkerMsg, ui::UI};
use super::uart::{self, UART};

pub struct State {
    ui: UI,
    uart: Option<UART>,
    state: Arc<Mutex<state::PersistentState>>,
}

impl State {
    fn handle_ui(&mut self, msg: Msg) -> Fallible<()> {
        match msg {
            Msg::Exit => Err(err_msg("UI thread requested exit")),

            Msg::Enumerate => {
                self.ui
                    .send(UIMsg::UARTsAvailable(uart::enumerate()))
                    .context("Error sending list of available UARTs")?;
                Ok(())
            }

            Msg::Connect(port) => {
                self.connect(&port);
                Ok(())
            }

            Msg::Disconnect => {
                self.disconnect();
                Ok(())
            }

            Msg::CtrlRunPath(steps, count) => {
                trace!("Sending run command");
                if let Err(e) = self
                    .uart
                    .as_mut()
                    .ok_or_else(|| err_msg("UART not connected"))
                    .and_then(|uart| {
                        uart.send(protocol::HostMsg::RunPattern(control::StepperPattern {
                            count,
                            steps,
                        }))
                    })
                {
                    report!(e.context("Error sending run command"))
                }
                Ok(())
            }

            Msg::CtrlPause => {
                trace!("Sending pause command");
                if let Err(e) = self
                    .uart
                    .as_mut()
                    .ok_or_else(|| err_msg("UART not connected"))
                    .and_then(|uart| uart.send(protocol::HostMsg::PausePattern))
                {
                    report!(e.context("Error sending pause command"))
                }
                Ok(())
            }

            Msg::CtrlStop => {
                trace!("Sending stop command");
                if let Err(e) = self
                    .uart
                    .as_mut()
                    .ok_or_else(|| err_msg("UART not connected"))
                    .and_then(|uart| uart.send(protocol::HostMsg::StopPattern))
                {
                    report!(e.context("Error sending stop command"))
                }
                Ok(())
            }

            Msg::UpdateConfig(cfg) => {
                self.ui.update_config(&cfg);
                Ok(())
            }
        }
    }

    fn connect(&mut self, port: &str) {
        let (uart, msg) = Connection::new(port)
            .and_then(|uart| {
                self.poll
                    .register(
                        &uart.uart,
                        UART_TOKEN,
                        Ready::readable()
                            | Ready::writable()
                            | UnixReady::error()
                            | UnixReady::hup(),
                        PollOpt::edge(),
                    )
                    .context("Error registering UART for polling")?;
                Ok(uart)
            })
            .map(|uart| (Some(uart), UIMsg::UARTConnected))
            .unwrap_or_else(|e| {
                report!(e.context("Error connecting to UART"));
                (None, UIMsg::UARTDisconnected)
            });
        self.uart = uart;
        if let Err(e) = self.ui.send(msg) {
            report!(e.context("Error notifying UI of connection"));
        }
    }

    fn disconnect(&mut self) {
        self.uart = None;
        if let Err(e) = self.ui.send(UIMsg::UARTDisconnected) {
            report!(e.context("Error notifying UI of disconnection"));
        }
    }

}
