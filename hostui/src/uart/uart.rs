use mio::{unix::UnixReady, Events, Ready};
use mio_serial::{DataBits, FlowControl, Parity, Serial, SerialPort, SerialPortSettings, StopBits};

pub fn enumerate() -> Vec<String> {
    trace!("Enumerating ports");
    mio_serial::available_ports()
        .map(|l| {
            l.iter().fold(Vec::<String>::new(), |mut v, p| {
                trace!("Found port {}", p.port_name);
                v.push(p.port_name.clone());
                v
            })
        })
        .unwrap_or_else(|e| {
            error!("Unable to enumerate ports: {:?}", e);
            Vec::<String>::new()
        })
}


struct UART {
    pub uart: Serial,
    ready: UnixReady,
    send_buf: Vec<u8>,
    decoder: protocol::MessageDecoder<Vec<u8>, Vec<usize>>,
}

impl UART {
    pub fn new(port: &str) -> Fallible<UART> {
        let mut uart = Serial::from_path(
            Path::new(port),
            &SerialPortSettings {
                baud_rate: 115_200,
                data_bits: DataBits::Eight,
                flow_control: FlowControl::None,
                parity: Parity::None,
                stop_bits: StopBits::One,
                timeout: Duration::new(1, 0),
            },
        )
        .with_context(|_| format!("Error opening UART {}", port))?;

        if let Err(e) = uart.set_exclusive(true) {
            warn!("Unable to set UART exclusive: {:?}", e);
        }

        let mut conn = UART {
            uart,
            ready: (Ready::readable() | Ready::writable()).into(),
            send_buf: Vec::new(),
            decoder: protocol::MessageDecoder::new(Vec::new(), Vec::new())?,
        };

        if let Err(e) = conn.uart.clear(mio_serial::ClearBuffer::All) {
            warn!("Unable to clear stale bytes from UART: {}", e);
        }
        if let Err(e) = conn.send(protocol::HostMsg::Hello) {
            Err(e
                .context("Error sending Hello after UART connection")
                .into())
        } else {
            conn.try_write()?;
            Ok(conn)
        }
    }

    pub fn ready(&mut self, ready: UnixReady) -> (bool, Fallible<()>) {
        self.ready = self.ready | ready;

        let read_res = self.try_read();
        let write_res = self.try_write();

        let disconnect = self.ready.is_error() || self.ready.is_hup();
        let err = (if self.ready.is_hup() {
            Err(err_msg("UART hung up"))
        } else {
            Ok(())
        })
        .and_then(|_| read_res)
        .and_then(|_| write_res)
        .and_then(|_| {
            if self.ready.is_error() {
                Err(err_msg("UART reported error"))
            } else {
                Ok(())
            }
        });

        (disconnect, err)
    }

    pub fn send(&mut self, msg: protocol::HostMsg) -> Fallible<()> {
        let bytes = protocol::encode_message(Vec::new(), &msg)?;
        self.send_buf.extend(bytes);
        self.try_write()?;

        Ok(())
    }

    pub fn recv_into(&mut self, ctx: &mut Context) -> Fallible<()> {
        while let Some(msg) = self.decoder.recv()? {
            ctx.emit_msg(msg)?;
        }
        Ok(())
    }

    fn try_read(&mut self) -> Fallible<()> {
        while Ready::from(self.ready).is_readable() {
            let mut bytes = [0u8; 64];
            let res = self.uart.read(&mut bytes);
            match res {
                Ok(0) => {
                    self.ready.remove(Ready::readable());
                    return Ok(());
                }

                Ok(count) => {
                    //trace!("Read {} bytes from UART", count);
                    self.decoder
                        .decode(&bytes[..count])
                        .context("Error decoding received message")?;
                }

                Err(err) => match err.kind() {
                    io::ErrorKind::WouldBlock => {
                        self.ready.remove(Ready::readable());
                        return Ok(());
                    }

                    _ => {
                        self.ready.remove(Ready::readable());
                        self.ready.insert(UnixReady::error());
                        Err(err).context("Error reading from UART")?;
                    }
                },
            }
        }
        Ok(())
    }

    fn try_write(&mut self) -> Fallible<()> {
        while !self.send_buf.is_empty() && Ready::from(self.ready).is_writable() {
            let res = self.uart.write(&self.send_buf);
            match res {
                Ok(0) => {
                    self.ready.remove(Ready::writable());
                    return Ok(());
                }

                Ok(count) => {
                    //trace!("Wrote {} bytes to UART", count);
                    self.send_buf.drain(..count);
                }

                Err(err) => {
                    self.ready.remove(Ready::writable());

                    match err.kind() {
                        io::ErrorKind::WouldBlock => {
                            return Ok(());
                        }

                        _ => {
                            self.ready.insert(UnixReady::error());
                            Err(err).context("Error writing to UART")?;
                        }
                    }
                }
            }
        }
        Ok(())
    }
}

impl Drop for UART {
    fn drop(&mut self) {
        if !self.ready.is_error() && !self.ready.is_hup() {
            let _ = self.send(protocol::HostMsg::Goodbye);
            /* This won't block, so we might not send everything; in principle, there's probably
             * enough buffer that it'll never matter.
             */
            let _ = self.try_write();
        }
    }
}
