#![allow(dead_code)]

use crate::ui::Msg as UIMsg;
use failure::Error;
use log::{Metadata, Record};
use relm;
use std::sync::Mutex;

pub struct Logger(Mutex<relm::Sender<UIMsg>>);

impl Logger {
    pub fn new(sender: relm::Sender<UIMsg>) -> Logger {
        Logger(Mutex::new(sender))
    }
}

impl ::log::Log for Logger {
    fn enabled(&self, _: &Metadata) -> bool {
        //metadata.level() >= Level::Trace
        true
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let s = format!(
                "{}({}): {}",
                record.level(),
                record.module_path().unwrap_or("unknown"),
                record.args()
            );
            let _ = self
                .0
                .lock()
                .map_err(|e| format!("Could not acquire log mutex: {:?}", e))
                .and_then(|g| {
                    g.send(UIMsg::LogMessage(s.clone()))
                        .map_err(|e| format!("Could not send log message: {:?}", e))
                })
                .map_err(|e| eprintln!("{}", e));
        }
    }

    fn flush(&self) {}
}

pub fn report_err(e: &Error) {
    error!(target: module_path!(), "{}", e);

    eprintln!("Error: {:?}", e);
    for cause in e.iter_causes() {
        eprintln!("  Caused by: {:?}", cause);
    }
}

macro_rules! report {
    ($e:expr) => {
        crate::logger::report_err(&($e).into())
    };
}
