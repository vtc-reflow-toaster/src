#[allow(unused_imports)]
#[macro_use]
extern crate log;

#[macro_use]
mod logger;
mod old_uart;
mod state;
mod ui;

type PersistentData = state::PersistentState;

fn main() {
    let state = state::PersistentState::new("config");
    ui::main::run(state);
}
