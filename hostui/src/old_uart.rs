// FIXME: this needs to be broken up

use crate::ui::Msg as UIMsg;
use common::protocol;
use failure::{err_msg, Fallible, ResultExt};
use mio::{unix::UnixReady, Events, Poll, PollOpt, Ready, Token};
use mio_extras::channel::Receiver;
use mio_serial::{DataBits, FlowControl, Parity, Serial, SerialPort, SerialPortSettings, StopBits};
use relm::Sender;
use std::io::{self, Read, Write};
use std::path::Path;
use std::time::Duration;

#[allow(dead_code)]
#[derive(Debug)]
pub enum Msg {
    Enumerate,
    Connect(String),
    Disconnect,

    SendMessage(protocol::HostMsg),

    Exit,
}

#[allow(dead_code)]
pub struct Context {
    ui_events: Sender<UIMsg>,
    uart_events: Receiver<Msg>,
}

impl Context {
    pub fn new(ui_events: Sender<UIMsg>, uart_events: Receiver<Msg>) -> Context {
        Context {
            ui_events,
            uart_events,
        }
    }

    pub fn send(&mut self, msg: UIMsg) -> Fallible<()> {
        self.ui_events
            .send(msg)
            .context("Error sending message to UI thread")?;
        Ok(())
    }

    pub fn recv(&mut self) -> Fallible<Msg> {
        Ok(self
            .uart_events
            .try_recv()
            .context("Error receiving message from UI thread")?)
    }

    fn emit_msg(&mut self, msg: protocol::CtrlMsg) -> Fallible<()> {
        use protocol::CtrlMsg::*;
        let event = match msg {
            Message(level, target, slice) => {
                UIMsg::LogMessage(format!("{}({}): {}", level.as_log_level(), target, slice))
            }
            InputOverflow => UIMsg::LogMessage("Controller: input buffer overflow".to_owned()),
            OutputOverflow => UIMsg::LogMessage("Controller: output buffer overflow".to_owned()),

            _ => UIMsg::ControllerMessage(unsafe { std::mem::transmute(msg) }),
        };
        self.send(event)
    }
}

struct Connection {
    uart: Serial,
    ready: UnixReady,
    send_buf: Vec<u8>,
    decoder: protocol::MessageDecoder<Vec<u8>, Vec<usize>>,
}

impl Connection {
    pub fn new(port: &str) -> Fallible<Connection> {
        let mut uart = Serial::from_path(
            Path::new(port),
            &SerialPortSettings {
                baud_rate: 115_200,
                data_bits: DataBits::Eight,
                flow_control: FlowControl::None,
                parity: Parity::None,
                stop_bits: StopBits::One,
                timeout: Duration::new(1, 0),
            },
        )
        .with_context(|_| format!("Error opening UART {}", port))?;

        if let Err(e) = uart.set_exclusive(true) {
            warn!("Unable to set UART exclusive: {:?}", e);
        }

        let mut conn = Connection {
            uart,
            ready: (Ready::readable() | Ready::writable()).into(),
            send_buf: Vec::new(),
            decoder: protocol::MessageDecoder::new(Vec::new(), Vec::new())?,
        };

        if let Err(e) = conn.uart.clear(mio_serial::ClearBuffer::All) {
            warn!("Unable to clear stale bytes from UART: {}", e);
        }
        if let Err(e) = conn.send(protocol::HostMsg::Hello) {
            Err(e
                .context("Error sending Hello after UART connection")
                .into())
        } else {
            conn.try_write()?;
            Ok(conn)
        }
    }

    pub fn ready(&mut self, ready: UnixReady) -> (bool, Fallible<()>) {
        self.ready = self.ready | ready;

        let read_res = self.try_read();
        let write_res = self.try_write();

        let disconnect = self.ready.is_error() || self.ready.is_hup();
        let err = (if self.ready.is_hup() {
            Err(err_msg("UART hung up"))
        } else {
            Ok(())
        })
        .and_then(|_| read_res)
        .and_then(|_| write_res)
        .and_then(|_| {
            if self.ready.is_error() {
                Err(err_msg("UART reported error"))
            } else {
                Ok(())
            }
        });

        (disconnect, err)
    }

    pub fn send(&mut self, msg: protocol::HostMsg) -> Fallible<()> {
        let bytes = protocol::encode_message(Vec::new(), &msg)?;
        self.send_buf.extend(bytes);
        self.try_write()?;

        Ok(())
    }

    pub fn recv_into(&mut self, ctx: &mut Context) -> Fallible<()> {
        while let Some(msg) = self.decoder.recv()? {
            ctx.emit_msg(msg)?;
        }
        Ok(())
    }

    fn try_read(&mut self) -> Fallible<()> {
        while Ready::from(self.ready).is_readable() {
            let mut bytes = [0u8; 64];
            let res = self.uart.read(&mut bytes);
            match res {
                Ok(0) => {
                    self.ready.remove(Ready::readable());
                    return Ok(());
                }

                Ok(count) => {
                    for b in bytes[..count].iter() {
                        if let Err(e) = self.decoder.add_byte(*b) {
                            error!("Error decoding received message: {:?}", e);
                        }
                    }
                }

                Err(err) => match err.kind() {
                    io::ErrorKind::WouldBlock => {
                        self.ready.remove(Ready::readable());
                        return Ok(());
                    }

                    _ => {
                        self.ready.remove(Ready::readable());
                        self.ready.insert(UnixReady::error());
                        Err(err).context("Error reading from UART")?;
                    }
                },
            }
        }
        Ok(())
    }

    fn try_write(&mut self) -> Fallible<()> {
        while !self.send_buf.is_empty() && Ready::from(self.ready).is_writable() {
            let res = self.uart.write(&self.send_buf);
            match res {
                Ok(0) => {
                    self.ready.remove(Ready::writable());
                    return Ok(());
                }

                Ok(count) => {
                    //trace!("Wrote {} bytes to UART", count);
                    self.send_buf.drain(..count);
                }

                Err(err) => {
                    self.ready.remove(Ready::writable());

                    match err.kind() {
                        io::ErrorKind::WouldBlock => {
                            return Ok(());
                        }

                        _ => {
                            self.ready.insert(UnixReady::error());
                            Err(err).context("Error writing to UART")?;
                        }
                    }
                }
            }
        }
        Ok(())
    }
}

impl Drop for Connection {
    fn drop(&mut self) {
        if !self.ready.is_error() && !self.ready.is_hup() {
            let _ = self.send(protocol::HostMsg::Goodbye);
            /* This won't block, so we might not send everything; in principle, there's probably
             * enough buffer that it'll never matter.
             */
            let _ = self.try_write();
        }
    }
}

struct State {
    ctx: Context,
    poll: Poll,
    conn: Option<Connection>,
}

const UI_TOKEN: Token = Token(0);
const UART_TOKEN: Token = Token(1);

impl State {
    fn new(ctx: Context) -> State {
        let poll = Poll::new()
            .map_err(|e| error!("Unable to create poll instance: {:?}", e))
            .unwrap();

        State {
            ctx,
            poll,
            conn: None,
        }
    }

    fn handle_ui(&mut self, msg: Msg) -> Fallible<()> {
        match msg {
            Msg::Exit => Err(err_msg("UI thread requested exit")),

            Msg::Enumerate => {
                self.ctx
                    .send(UIMsg::UARTAvailable(enumerate()))
                    .context("Error sending list of available UARTs")?;
                Ok(())
            }

            Msg::Connect(port) => {
                self.connect(&port);
                Ok(())
            }

            Msg::Disconnect => {
                self.disconnect();
                Ok(())
            }

            Msg::SendMessage(msg) => {
                trace!("Sending command {:#?}", msg);
                if let Err(e) = self
                    .conn
                    .as_mut()
                    .ok_or_else(|| err_msg("UART not connected"))
                    .and_then(|conn| conn.send(msg))
                {
                    report!(e.context("Error sending command"));
                }
                Ok(())
            }
        }
    }

    fn connect(&mut self, port: &str) {
        let (conn, msg) = Connection::new(port)
            .and_then(|conn| {
                self.poll
                    .register(
                        &conn.uart,
                        UART_TOKEN,
                        Ready::readable()
                            | Ready::writable()
                            | UnixReady::error()
                            | UnixReady::hup(),
                        PollOpt::edge(),
                    )
                    .context("Error registering UART for polling")?;
                Ok(conn)
            })
            .map(|conn| (Some(conn), UIMsg::UARTConnected))
            .unwrap_or_else(|e| {
                report!(e.context("Error connecting to UART"));
                (None, UIMsg::UARTDisconnected)
            });
        self.conn = conn;
        if let Err(e) = self.ctx.send(msg) {
            report!(e.context("Error notifying UI of connection"));
        }
    }

    fn disconnect(&mut self) {
        self.conn = None;
        if let Err(e) = self.ctx.send(UIMsg::UARTDisconnected) {
            report!(e.context("Error notifying UI of disconnection"));
        }
    }

    fn run(&mut self) {
        self.poll
            .register(
                &self.ctx.uart_events,
                UI_TOKEN,
                Ready::readable(),
                PollOpt::level(),
            )
            .map_err(|e| error!("Unable to attach UI to poll instance: {:?}", e))
            .unwrap();

        let mut events = Events::with_capacity(32);
        trace!("Entering UART poll loop");
        'main: loop {
            self.poll
                .poll(&mut events, None)
                .map_err(|e| error!("Unable to poll: {:?}", e))
                .unwrap();

            for event in events.iter() {
                match event.token() {
                    UI_TOKEN => {
                        if self.ctx.recv().and_then(|msg| self.handle_ui(msg)).is_err() {
                            break 'main;
                        }
                    }

                    UART_TOKEN => {
                        let conn = self.conn.as_mut().unwrap();
                        let (disconnect, err) = conn.ready(event.readiness().into());

                        if let Err(e) = err {
                            report!(e.context("Error polling UART"));
                        }

                        if let Err(e) = conn.recv_into(&mut self.ctx) {
                            report!(e.context("Error handling received messages"));
                        }

                        if disconnect {
                            self.disconnect();
                        }
                    }

                    _ => {
                        report!(err_msg("Unknown event token received"));
                        panic!();
                    }
                };
            }
        }
    }
}

fn enumerate() -> Vec<String> {
    trace!("Enumerating ports");
    mio_serial::available_ports()
        .map(|l| {
            l.iter().fold(Vec::<String>::new(), |mut v, p| {
                trace!("Found port {}", p.port_name);
                v.push(p.port_name.clone());
                v
            })
        })
        .unwrap_or_else(|e| {
            error!("Unable to enumerate ports: {:?}", e);
            Vec::<String>::new()
        })
}

pub fn uart_thread(ctx: Context) {
    trace!("Entered UART thread");

    let mut state = State::new(ctx);
    state.run();

    trace!("Shutting down");
}
