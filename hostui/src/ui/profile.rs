use super::{main::Win, Model, Msg};
use gtk::prelude::*;
use relm::{connect, Relm};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct ProfileData<T: Clone> {
    map: HashMap<String, T>,
}

impl<T: Clone> ProfileData<T> {
    pub fn get_active(&self, c: &ProfileCluster) -> Option<T> {
        info!("Loading profile {}", c.active());
        self.map.get(&c.active()).cloned()
    }

    pub fn set_active(&mut self, c: &mut ProfileCluster, v: &T) {
        info!("Saving profile {}", c.active());
        self.map.insert(c.active(), v.clone());
        c.populate(self.map.keys());
    }

    pub fn del_active(&mut self, c: &mut ProfileCluster) {
        info!("Deleting profile {}", c.active());
        self.map.remove(&c.active());
        c.populate(self.map.keys());
    }

    pub fn feed(&self, c: &mut ProfileCluster) {
        c.populate(self.map.keys());
    }
}

pub struct ProfileCluster {
    selection: gtk::ComboBoxText,
}

impl ProfileCluster {
    fn active(&self) -> String {
        self.selection
            .get_active_text()
            .map(|g| g.into())
            .unwrap_or_else(String::new)
    }

    fn populate<'a>(&mut self, items: impl Iterator<Item = &'a String>) {
        let selected = self.active();
        self.selection.remove_all();
        for (idx, item) in items.enumerate() {
            self.selection.append_text(item);
            if *item == selected {
                self.selection
                    .clone()
                    .upcast::<gtk::ComboBox>()
                    .set_active(Some(idx as u32));
            }
        }
    }

    pub fn new(relm: &Relm<Win>, parent: &impl gtk::ContainerExt, _: &Model) -> Self {
        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        parent.add(&hbox);

        let selection = gtk::ComboBoxText::new_with_entry();
        selection.set_size_request(200, -1);
        hbox.add(&selection);

        let save_btn = gtk::Button::new_with_label("Save");
        connect!(relm, save_btn, connect_clicked(_), Msg::ProfileSave);
        hbox.add(&save_btn);

        let load_btn = gtk::Button::new_with_label("Load");
        connect!(relm, load_btn, connect_clicked(_), Msg::ProfileLoad);
        hbox.add(&load_btn);

        let del_btn = gtk::Button::new_with_label("Delete");
        connect!(relm, del_btn, connect_clicked(_), Msg::ProfileDel);
        hbox.add(&del_btn);

        ProfileCluster { selection }
    }
}
