#![allow(dead_code)]

use super::operation::{self, OperationPanel};
use super::{Model, Msg, UARTMsg, UARTStatus};
use crate::{logger, old_uart};
use common::protocol::{HostMsg, CtrlMsg};
use gtk::prelude::*;
use relm::{connect, connect_stream, Relm, Update, Widget};
use std::collections::HashMap;

macro_rules! button {
    ($relm:expr, $label:expr, $msg:expr) => {{
        let btn = gtk::Button::new_with_label($label);
        connect!($relm, btn, connect_clicked(_), $msg);
        btn
    }};
}

pub struct Win {
    model: Model,

    window: gtk::Window,

    uart_list: gtk::ComboBoxText,
    uart_connect: gtk::Button,
    uart_disconnect: gtk::Button,
    uart_refresh: gtk::Button,

    temp_display: gtk::Label,

    control_start: gtk::Button,
    control_stop: gtk::Button,

    op_panels: HashMap<String, Box<OperationPanel>>,
    op_stack: gtk::Stack,

    log_text: gtk::TextView,
}

impl Win {
    fn active_panel_name(&self) -> String {
        self.op_stack.get_visible_child_name().unwrap().to_string()
    }

    fn panel_call<R>(
        &self,
        name: &str,
        f: impl FnOnce(&Model, &dyn OperationPanel) -> R,
    ) -> Option<R> {
        let model = &self.model;
        let panel = self.op_panels.get(name);
        panel.map(|p| f(model, p.as_ref()))
    }

    fn active_panel_call<R>(&self, f: impl FnOnce(&Model, &dyn OperationPanel) -> R) -> R {
        let name = self.active_panel_name();
        self.panel_call(&name, f).unwrap()
    }

    fn panel_call_mut<R>(
        &mut self,
        name: &str,
        f: impl FnOnce(&mut Model, &mut dyn OperationPanel) -> R,
    ) -> Option<R> {
        let model = &mut self.model;
        let panel = self.op_panels.get_mut(name);
        panel.map(|p| f(model, p.as_mut()))
    }

    fn active_panel_call_mut<R>(
        &mut self,
        f: impl FnOnce(&mut Model, &mut dyn OperationPanel) -> R,
    ) -> R {
        let name = self.active_panel_name();
        self.panel_call_mut(&name, f).unwrap()
    }

    fn update_active(&mut self, event: &Msg) {
        self.active_panel_call_mut(|m, p| p.handle_event(m, event))
    }

    fn active_can_execute(&self) -> bool {
        self.active_panel_call(|_, p| p.can_execute())
    }

    fn update_sensitivity(&mut self) {
        let connected = self.model.uart_status == UARTStatus::Connected;
        let disconnected = self.model.uart_status == UARTStatus::Disconnected;

        if !connected {
            self.model.running_operation_owner = None;
            self.temp_display.set_text("Not connected");
        }

        self.uart_list.set_sensitive(disconnected);
        self.uart_connect.set_sensitive(disconnected);
        self.uart_disconnect.set_sensitive(connected);
        self.uart_refresh.set_sensitive(disconnected);
        let active_can_execute = self.active_can_execute();
        self.control_start.set_sensitive(
            connected && self.model.running_operation_owner.is_none() && active_can_execute,
        );
        self.control_stop
            .set_sensitive(connected && self.model.running_operation_owner.is_some());
    }
}

impl Update for Win {
    type Model = Model;
    type ModelParam = crate::PersistentData;
    type Msg = Msg;

    fn model(relm: &Relm<Self>, persistent: crate::PersistentData) -> Model {
        let log_stream = relm.stream().clone();
        let (_log_channel, log_sender) = relm::Channel::new(move |msg| log_stream.emit(msg));
        log::set_boxed_logger(Box::new(logger::Logger::new(log_sender)))
            .map(|()| log::set_max_level(log::LevelFilter::Trace))
            .expect("Unable to set logger");
        trace!("Initialized logger");

        let uart_stream = relm.stream().clone();
        let (_uart_channel, uart_ui_sender) = relm::Channel::new(move |msg| uart_stream.emit(msg));

        let (uart_sender, uart_receiver) = mio_extras::channel::channel();
        let uart_context = old_uart::Context::new(uart_ui_sender, uart_receiver);
        let uart_thread = Some(std::thread::spawn(move || {
            old_uart::uart_thread(uart_context)
        }));
        trace!("Created UART thread");

        if let Err(e) = uart_sender.send(UARTMsg::Enumerate) {
            error!("Could not request new UART list: {:?}", e);
        }

        Model {
            running_operation_owner: None,
            uart_sender,
            uart_thread,
            uart_status: UARTStatus::Disconnected,
            persistent,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Quit => {
                self.model.persistent.save_now();
                let _ = self.model.uart_sender.send(UARTMsg::Exit);
                self.model.uart_thread.take().map(|t| t.join());
                gtk::main_quit();
            }

            Msg::LogMessage(m) => {
                self.log_text
                    .get_buffer()
                    .map(|b| {
                        let end = &mut b.get_end_iter();
                        b.insert(end, &m);
                        b.insert(end, "\n");
                        let mark = b.get_mark("end").unwrap();
                        self.log_text.scroll_mark_onscreen(&mark);
                    })
                    .unwrap();
            }

            Msg::UARTAvailable(v) => {
                let list = &mut self.uart_list;
                let selected = list.get_active_text();
                list.remove_all();
                for port in &v {
                    list.append_text(port);
                }
                if v.is_empty() {
                    let idx = selected
                        .and_then(|s| v.iter().position(|p| *p == s))
                        .unwrap_or(0);
                    list.clone()
                        .upcast::<gtk::ComboBox>()
                        .set_active(Some(idx as u32));
                }
            }

            Msg::UARTConnect => {
                let selected = self.uart_list.get_active_text();
                if let Some(p) = selected {
                    info!("Requesting connection to {}", p);
                    if let Err(e) = self.model.uart_sender.send(UARTMsg::Connect(p.to_string())) {
                        error!("Could request connection to UART {}: {:?}", p, e);
                    }
                    self.model.uart_status = UARTStatus::Connecting;
                    self.update_sensitivity();
                } else {
                    info!("No port selected; not connecting");
                }
            }

            Msg::UARTConnected => {
                self.model.uart_status = UARTStatus::Connected;
                self.update_sensitivity();
            }

            Msg::UARTDisconnect => {
                info!("Disconnecting UART");
                if let Err(e) = self.model.uart_sender.send(UARTMsg::Disconnect) {
                    error!("Could not request disconnection: {:?}", e);
                }
                self.model.uart_status = UARTStatus::Disconnecting;
                self.update_sensitivity();
            }

            Msg::UARTDisconnected => {
                self.model.uart_status = UARTStatus::Disconnected;
                self.update_sensitivity();
            }

            Msg::UARTRefresh => {
                if let Err(e) = self.model.uart_sender.send(UARTMsg::Enumerate) {
                    error!("Could not request new UART list: {:?}", e);
                }
            }

            Msg::FocusChanged => {
                self.update_active(&event);
                self.update_sensitivity();
            }

            //Msg::ControllerMessage(_) => self.update_active(&event),
            Msg::ControllerMessage(ref msg) => {
                //info!("Handling message {:?}", msg);
                match msg {
                    &CtrlMsg::Running => {
                        self.model.running_operation_owner.get_or_insert("");
                        self.update_sensitivity();
                    }

                    &CtrlMsg::Stopped => {
                        self.model.running_operation_owner.take();
                        self.update_sensitivity();
                    }

                    &CtrlMsg::Temperature(temp) => {
                        let text = match temp {
                            Some(temp) => format!("Temperature: {}°C", temp.degrees()),
                            None => "Unknown temperature".to_owned()
                        };
                        self.temp_display.set_text(&text);
                    }

                    _ => (),
                };

                if let Some(p) = self.model.running_operation_owner.filter(|p| !p.is_empty()) {
                    //info!("Handing message off to {}", p);
                    self.panel_call_mut(p, |m, p| p.handle_event(m, &event));
                }
            }

            Msg::ControlStart => {
                if let Some(msg) = self.active_panel_call(|m, p| p.get_operation(m)) {
                    if let Err(e) = self
                        .model
                        .uart_sender
                        .send(UARTMsg::SendMessage(HostMsg::RunOperation(msg)))
                    {
                        error!("Failed to start operation: {:?}", e);
                    } else {
                        //self.running_operation_owner = Some(self.active_panel_name());
                        let active = self.active_panel_call(|_, p| p.name());
                        self.model.running_operation_owner = Some(active);
                    }
                } else {
                    error!("No operation returned from panel");
                }
            }

            Msg::ControlStop => {
                if let Err(e) = self
                    .model
                    .uart_sender
                    .send(UARTMsg::SendMessage(HostMsg::AbortOperation))
                {
                    error!("Failed to abort operation: {:?}", e);
                }
            }

            // XXX should this notify all panels?
            Msg::StateUpdated => self.update_active(&event),

            Msg::Redraw => self.update_active(&event),

            Msg::ProfileDel | Msg::ProfileSave => {
                self.update_active(&event);
                self.model.persistent.save_now();
            }

            Msg::ProfileLoad => self.update_active(&event),
        }
    }
}

impl Widget for Win {
    type Root = gtk::Window;

    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let window = gtk::Window::new(gtk::WindowType::Toplevel);
        window.set_title("Reflow Toaster");
        connect!(
            relm,
            window,
            connect_delete_event(_, _),
            return (Msg::Quit, Inhibit(false))
        );

        let vbox_top = gtk::Box::new(gtk::Orientation::Vertical, 5);
        window.add(&vbox_top);

        let hbox_uart = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        let uart_list = gtk::ComboBoxText::new();
        uart_list.set_size_request(200, -1);
        hbox_uart.add(&uart_list);
        let uart_refresh = button!(relm, "Refresh", Msg::UARTRefresh);
        hbox_uart.add(&uart_refresh);
        let uart_connect = button!(relm, "Connect", Msg::UARTConnect);
        hbox_uart.add(&uart_connect);
        let uart_disconnect = button!(relm, "Disconnect", Msg::UARTDisconnect);
        hbox_uart.add(&uart_disconnect);
        let spacer = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        spacer.set_hexpand(true);
        hbox_uart.add(&spacer);

        let temp_display = gtk::Label::new("Not connected");
        temp_display.set_size_request(200, -1);
        hbox_uart.add(&temp_display);

        let spacer = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        spacer.set_hexpand(true);
        hbox_uart.add(&spacer);

        let control_start = button!(relm, "Start", Msg::ControlStart);
        hbox_uart.add(&control_start);
        let control_stop = button!(relm, "Stop", Msg::ControlStop);
        hbox_uart.add(&control_stop);
        vbox_top.add(&hbox_uart);

        let mut op_panels = HashMap::<String, Box<OperationPanel>>::new();
        let hbox_op = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        let op_stack = gtk::Stack::new();
        op_stack.set_vexpand(true);
        op_stack.set_hexpand(true);
        connect!(
            relm,
            op_stack,
            connect_property_visible_child_notify(_),
            Msg::FocusChanged
        );

        let (jog_panel, jog_box) = operation::jog_door::JogDoorPanel::new();
        op_stack.add_titled(&jog_box, jog_panel.name(), jog_panel.name());
        op_panels.insert(jog_panel.name().to_owned(), Box::new(jog_panel));

        let (temp_panel, temp_box) = operation::temp_profile::TempProfilePanel::new(relm, &model);
        op_stack.add_titled(&temp_box, temp_panel.name(), temp_panel.name());
        op_panels.insert(temp_panel.name().to_owned(), Box::new(temp_panel));

        let (bb_panel, bb_box) = operation::bang_bang::BangBangPanel::new(relm, &model);
        op_stack.add_titled(&bb_box, bb_panel.name(), bb_panel.name());
        op_panels.insert(bb_panel.name().to_owned(), Box::new(bb_panel));

        /*
        let stack_switch = gtk::StackSwitcher::new();
        stack_switch.set_stack(Some(&op_stack));
        vbox_top.add(&stack_switch);
        vbox_top.add(&op_stack);
        */

        let panel_sidebar = gtk::StackSidebar::new();
        panel_sidebar.set_property_stack(Some(&op_stack));
        panel_sidebar.set_vexpand(true);
        hbox_op.add(&panel_sidebar);
        hbox_op.add(&op_stack);

        vbox_top.add(&hbox_op);

        let log_scroll = gtk::ScrolledWindow::new(
            Option::<&gtk::Adjustment>::None,
            Option::<&gtk::Adjustment>::None,
        );
        log_scroll.set_size_request(-1, 100);

        let log_text = gtk::TextView::new();
        log_text.set_editable(false);
        log_text.set_cursor_visible(false);
        log_text
            .get_buffer()
            .map(|b| b.create_mark("end", &b.get_end_iter(), false))
            .unwrap();
        log_scroll.add(&log_text);

        let log_frame = gtk::Frame::new("Application log");
        log_frame.add(&log_scroll);
        vbox_top.add(&log_frame);

        window.show_all();

        let mut ret = Self {
            model,

            window,

            uart_list,
            uart_connect,
            uart_disconnect,
            uart_refresh,

            temp_display,

            control_start,
            control_stop,

            op_panels,
            op_stack,

            log_text,
        };

        ret.update(Msg::UARTDisconnected);
        ret.update(Msg::FocusChanged);

        ret
    }
}

pub fn run(model: crate::PersistentData) {
    Win::run(model).unwrap();
}
