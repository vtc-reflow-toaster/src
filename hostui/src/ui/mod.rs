mod graph;
pub mod main;
pub mod operation;
mod profile;

pub use crate::old_uart::Msg as UARTMsg;
pub use common::protocol::CtrlMsg;
use relm_derive::Msg;

#[derive(PartialEq, Eq, Clone, Copy)]
enum UARTStatus {
    Disconnected,
    Connecting,
    Connected,
    Disconnecting,
}

pub struct Model {
    running_operation_owner: Option<&'static str>,

    uart_sender: mio_extras::channel::Sender<UARTMsg>,
    uart_thread: Option<std::thread::JoinHandle<()>>,
    uart_status: UARTStatus,

    persistent: crate::PersistentData,
}

#[derive(Msg, Debug)]
pub enum Msg {
    Quit,
    Redraw,

    LogMessage(String),

    StateUpdated,
    FocusChanged,

    // NB: cannot safely hold strings; must contain log messages
    ControllerMessage(CtrlMsg<'static>),

    ProfileLoad,
    ProfileSave,
    ProfileDel,

    UARTRefresh,
    UARTAvailable(Vec<String>),
    UARTConnect,
    UARTConnected,
    UARTDisconnect,
    UARTDisconnected,

    ControlStart,
    ControlStop,
}
