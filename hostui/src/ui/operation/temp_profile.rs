use super::{OperationPanel, profile_graph::ProfileGraphModel};
use crate::ui::{graph, main::Win, profile, Model, Msg};
use common::types::temperature::*;
use graph::Model as _;
use gtk::prelude::*;
use relm::{connect, connect_stream, Relm};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct TempProfileData {
    model: ProfileGraphModel,
    profiles: profile::ProfileData<TemperatureProfile>,
}

impl TempProfileData {
    pub fn get_profile(&self) -> TemperatureProfile {
        self.model.get_profile().clone()
    }
}

pub struct TempProfilePanel {
    profiles: profile::ProfileCluster,

    graph_area: gtk::DrawingArea,
    graph_view: graph::View,

    soak_ramp_rate: gtk::SpinButton,
    soak_ramp_target: gtk::SpinButton,

    soak_time: gtk::SpinButton,
    soak_target: gtk::SpinButton,

    reflow_ramp_rate: gtk::SpinButton,
    liquidus_temp: gtk::SpinButton,
    time_above_liquidus: gtk::SpinButton,
    temp_limit: gtk::SpinButton,
    cool_rate: gtk::SpinButton,
    cool_target: gtk::SpinButton,
}

macro_rules! spin {
    ($relm:expr, $box:expr, $label:expr, $min:expr, $max:expr, $inc:expr) => {{
        let label = gtk::Label::new($label);
        $box.add(&label);
        let sb = gtk::SpinButton::new_with_range($min, $max, $inc);
        connect!(
            $relm,
            sb,
            connect_property_value_notify(_),
            Msg::StateUpdated
        );
        $box.add(&sb);
        sb
    }};
}

impl TempProfilePanel {
    pub fn new(relm: &Relm<Win>, model: &Model) -> (Self, gtk::Box) {
        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
        let mut profiles = profile::ProfileCluster::new(relm, &vbox, model);
        model.persistent.temp_profile.profiles.feed(&mut profiles);

        let graph_area = gtk::DrawingArea::new();
        graph_area.set_size_request(100, 200);
        graph_area.set_hexpand(true);
        graph_area.set_vexpand(true);
        connect!(
            relm,
            graph_area,
            connect_draw(_, _),
            return (Msg::Redraw, Inhibit(false))
        );
        let graph_frame = gtk::Frame::new("Temperature Profile");
        graph_frame.add(&graph_area);
        vbox.add(&graph_frame);

        let mut graph_view = graph::View::new();
        graph_view.init(&graph_area);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let soak_ramp_rate = spin!(relm, hbox, "Preheat rate", 0.5, 10.0, 0.1);
        let soak_ramp_target = spin!(relm, hbox, "Preheat final temperature", 25.0, 300.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let soak_time = spin!(relm, hbox, "Soak time", 0.0, 120.0, 1.0);
        let soak_target = spin!(relm, hbox, "Soak final temperature", 25.0, 300.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let reflow_ramp_rate = spin!(relm, hbox, "Final heat rate", 0.5, 10.0, 0.1);
        let temp_limit = spin!(relm, hbox, "Maximum temperature", 100.0, 300.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let liquidus_temp = spin!(relm, hbox, "Liquidus temperature", 25.0, 300.0, 1.0);
        let time_above_liquidus = spin!(relm, hbox, "Time above liquidus", 0.0, 120.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let cool_rate = spin!(relm, hbox, "Cool rate", 0.5, 10.0, 0.1);
        let cool_target = spin!(relm, hbox, "Cool target temperature", 25.0, 300.0, 1.0);

        let mut ret = Self {
            profiles,

            graph_area,
            graph_view,

            soak_ramp_target,
            soak_ramp_rate,

            soak_time,
            soak_target,

            reflow_ramp_rate,
            liquidus_temp,
            time_above_liquidus,
            temp_limit,

            cool_rate,
            cool_target,
        };

        ret.update_buttons(model.persistent.temp_profile.model.get_profile());
        (ret, vbox)
    }

    fn queue_redraw_graph(&self) {
        self.graph_area.queue_draw();
    }

    fn redraw_graph(&mut self, model: &mut TempProfileData) {
        self.graph_view.draw(&model.model, &mut self.graph_area);
        model.model.did_redraw();
    }

    fn update_buttons(&mut self, profile: &TemperatureProfile) {
        self.soak_ramp_target
            .set_value(profile.soak_ramp_target.into());
        self.soak_ramp_rate.set_value(profile.soak_ramp_rate.into());

        self.soak_time.set_value(profile.soak_time.into());
        self.soak_target.set_value(profile.soak_target.into());

        self.reflow_ramp_rate
            .set_value(profile.reflow_ramp_rate.into());
        self.liquidus_temp.set_value(profile.liquidus_temp.into());
        self.time_above_liquidus
            .set_value(profile.time_above_liquidus.into());
        self.temp_limit
            .set_value(profile.temp_limit.into());

        self.cool_rate.set_value(profile.cool_rate.into());
        self.cool_target.set_value(profile.cool_target.into());
    }

    fn update_profile(&self, profile: &mut TemperatureProfile) {
        profile.soak_ramp_target = self.soak_ramp_target.get_value().into();
        profile.soak_ramp_rate = self.soak_ramp_rate.get_value().into();

        profile.soak_time = self.soak_time.get_value().into();
        profile.soak_target = self.soak_target.get_value().into();

        profile.reflow_ramp_rate = self.reflow_ramp_rate.get_value().into();
        profile.liquidus_temp = self.liquidus_temp.get_value().into();
        profile.time_above_liquidus = self.time_above_liquidus.get_value().into();
        profile.temp_limit = self.temp_limit.get_value().into();

        profile.cool_rate = self.cool_rate.get_value().into();
        profile.cool_target = self.cool_target.get_value().into();
    }
}

impl OperationPanel for TempProfilePanel {
    fn name(&self) -> &'static str {
        "Temperature Profile"
    }

    fn handle_event(&mut self, model: &mut Model, event: &Msg) {
        let pdata = &mut model.persistent.temp_profile;
        match event {
            Msg::ProfileDel => pdata.profiles.del_active(&mut self.profiles),

            Msg::ProfileLoad => {
                if let Some(profile) = pdata.profiles.get_active(&self.profiles) {
                    self.update_buttons(&profile);
                    self.queue_redraw_graph();
                    *pdata.model.get_profile_mut() = profile;
                }
            }

            Msg::ProfileSave => {
                let profile = pdata.model.get_profile();
                pdata.profiles.set_active(&mut self.profiles, profile);
            }

            Msg::StateUpdated => {
                self.update_profile(pdata.model.get_profile_mut());
                self.queue_redraw_graph();
            }

            Msg::Redraw => self.redraw_graph(pdata),

            Msg::FocusChanged => (),

            Msg::ControllerMessage(_) => (),

            _ => (),
        }
    }
}
