use super::OperationPanel;
use crate::ui::{Model, Msg};
use common::operation::*;
use gtk::prelude::*;

pub struct JogDoorPanel {
    counts_button: gtk::SpinButton,
}

impl JogDoorPanel {
    pub fn new() -> (Self, gtk::Box) {
        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        hbox.add(&gtk::Label::new("Counts:"));
        let spin = gtk::SpinButton::new_with_range(-4096.0, 4096.0, 1.0);
        spin.set_value(0.0);
        hbox.add(&spin);
        vbox.add(&hbox);
        (
            Self {
                counts_button: spin,
            },
            vbox,
        )
    }
}

impl OperationPanel for JogDoorPanel {
    fn name(&self) -> &'static str {
        "Jog Door"
    }

    fn can_execute(&self) -> bool {
        true
    }

    fn get_operation(&self, _model: &Model) -> Option<Operation> {
        let counts = self.counts_button.get_value() as i32;
        Some(jog_door::JogDoor::new(counts).into())
    }

    fn handle_event(&mut self, _model: &mut Model, _event: &Msg) {}
}
