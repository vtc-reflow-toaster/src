use super::{Model, Msg};
use common::operation::Operation;

pub mod bang_bang;
pub mod jog_door;
pub mod temp_profile;
mod profile_graph;

pub trait OperationPanel {
    fn name(&self) -> &'static str;

    fn can_execute(&self) -> bool {
        false
    }

    // XXX this may not be needed; panels should respond to a focus event instead?
    /*
    fn can_simulate(&self) -> bool {
        false
    }
    */

    fn get_operation(&self, _model: &Model) -> Option<Operation> {
        None
    }

    fn handle_event(&mut self, _model: &mut Model, _event: &Msg);
}
