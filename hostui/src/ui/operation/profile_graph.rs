use crate::ui::graph::{Axis, Model, PathInfo, Point};
use common::types::temperature::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug, Hash, PartialEq, Eq)]
pub enum Series {
    Reported,
    Secondary,
    Simulated,
}

#[derive(Serialize, Deserialize, Default, Debug)]
#[serde(default)]
pub struct ProfileGraphModel {
    #[serde(skip)]
    dirty: bool,
    series: HashMap<Series, Vec<Point>>,
    profile: TemperatureProfile,
}

impl ProfileGraphModel {
    pub fn get_profile(&self) -> &TemperatureProfile {
        &self.profile
    }

    pub fn get_profile_mut(&mut self) -> &mut TemperatureProfile {
        self.dirty = true;
        &mut self.profile
    }

    #[allow(dead_code)]
    pub fn get_points(&self, series: Series) -> Vec<Point> {
        self.series.get(&series).cloned().unwrap_or_else(Vec::new)
    }

    pub fn get_points_mut(&mut self, series: Series) -> &mut Vec<Point> {
        self.dirty = true;
        self.series.entry(series).or_default()
    }

    #[allow(dead_code)]
    pub fn clear_points(&mut self, series: Series) {
        self.dirty = true;
        self.get_points_mut(series).clear();
    }

    pub fn clear_all_points(&mut self) {
        self.dirty = true;
        self.series.clear();
    }

    pub fn add_point(&mut self, s: Series, i: Interval, t: Temperature) {
        self.get_points_mut(s).push(Point(i.into(), t.into()));
    }

    fn profile_points(&self) -> Vec<Point> {
        self.profile
            .point_vec()
            .iter()
            .map(|(i, t)| Point((*i).into(), (*t).into()))
            .collect()
    }

    #[allow(dead_code)]
    pub fn request_redraw(&mut self) {
        self.dirty = true
    }
}

impl Model for ProfileGraphModel {
    fn needs_redraw(&self) -> bool {
        self.dirty
    }

    fn did_redraw(&mut self) {
        self.dirty = false
    }

    fn axes(&self) -> (Axis, Axis) {
        let (max_x, max_y) = self
            .profile_points()
            .iter()
            .chain(self.series.iter().map(|(_, v)| v.iter()).flatten())
            .fold((0.0f64, 0.0f64), |(max_x, max_y), &Point(i, t)| {
                (max_x.max(i), max_y.max(t))
            });

        (
            Axis {
                scale: 0.0..max_x,
                ..Default::default()
            },
            Axis {
                scale: 0.0..max_y,
                ..Default::default()
            },
        )
    }

    fn all_paths(&self) -> Vec<(PathInfo, Vec<Point>)> {
        let mut vec = Vec::with_capacity(3);
        vec.push((
            PathInfo {
                ..Default::default()
            },
            self.profile_points(),
        ));
        for (k, v) in self.series.iter() {
            let color = match *k {
                Series::Reported => [0.8, 0.35, 0.35, 1.0],
                Series::Secondary => [0.35, 0.35, 0.8, 1.0],
                Series::Simulated => [0.35, 0.8, 0.35, 1.0],
            };

            vec.push((
                PathInfo {
                    color,
                    ..Default::default()
                },
                v.clone(),
            ));
        }

        vec
    }
}
