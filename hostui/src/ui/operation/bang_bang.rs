use super::{profile_graph::{self, ProfileGraphModel}, OperationPanel};
use crate::ui::{graph, main::Win, profile, Model, Msg};
use common::operation::bang_bang_control::BangBangControl;
use common::operation::Operation;
use common::protocol::CtrlMsg;
use common::types::temperature::*;
use graph::Model as _;
use gtk::prelude::*;
use relm::{connect, connect_stream, Relm};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(default)]
struct BangBangSettings {
    look_ahead: Interval,

    top_hyst_on: TemperatureDelta,
    top_hyst_off: TemperatureDelta,
    bottom_hyst_on: TemperatureDelta,
    bottom_hyst_off: TemperatureDelta,

    door_open_position: i32,
    door_hyst_open: TemperatureDelta,
    door_hyst_close: TemperatureDelta,
}

impl BangBangSettings {
    fn to_control(&self, profile: &TemperatureProfile) -> BangBangControl {
        BangBangControl::new(
            profile,
            self.look_ahead,
            self.top_hyst_on,
            self.top_hyst_off,
            self.bottom_hyst_on,
            self.bottom_hyst_off,
            self.door_open_position,
            self.door_hyst_open,
            self.door_hyst_close,
        )
    }
}

impl Default for BangBangSettings {
    fn default() -> Self {
        Self {
            look_ahead: Interval::from_seconds(30),

            top_hyst_on: TemperatureDelta::new(-10),
            top_hyst_off: TemperatureDelta::new(0),
            bottom_hyst_on: TemperatureDelta::new(-5),
            bottom_hyst_off: TemperatureDelta::new(0),

            door_open_position: 2048,
            door_hyst_open: TemperatureDelta::new(-2),
            door_hyst_close: TemperatureDelta::new(2),
        }
    }
}

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct BangBangData {
    graph_model: ProfileGraphModel,
    active: BangBangSettings,
    profiles: profile::ProfileData<BangBangSettings>,
}

pub struct BangBangPanel {
    profiles: profile::ProfileCluster,

    graph_area: gtk::DrawingArea,
    graph_view: graph::View,

    look_ahead: gtk::SpinButton,

    top_hyst_on: gtk::SpinButton,
    top_hyst_off: gtk::SpinButton,
    bottom_hyst_on: gtk::SpinButton,
    bottom_hyst_off: gtk::SpinButton,

    door_open_position: gtk::SpinButton,
    door_hyst_open: gtk::SpinButton,
    door_hyst_close: gtk::SpinButton,
}

macro_rules! spin {
    ($relm:expr, $box:expr, $label:expr, $min:expr, $max:expr, $inc:expr) => {{
        let label = gtk::Label::new($label);
        $box.add(&label);
        let sb = gtk::SpinButton::new_with_range($min, $max, $inc);
        connect!(
            $relm,
            sb,
            connect_property_value_notify(_),
            Msg::StateUpdated
        );
        $box.add(&sb);
        sb
    }};
}

impl BangBangPanel {
    pub fn new(relm: &Relm<Win>, model: &Model) -> (Self, gtk::Box) {
        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
        let mut profiles = profile::ProfileCluster::new(relm, &vbox, model);
        model.persistent.bang_bang.profiles.feed(&mut profiles);

        let graph_area = gtk::DrawingArea::new();
        graph_area.set_size_request(100, 200);
        graph_area.set_hexpand(true);
        graph_area.set_vexpand(true);
        connect!(
            relm,
            graph_area,
            connect_draw(_, _),
            return (Msg::Redraw, Inhibit(false))
        );
        let graph_frame = gtk::Frame::new("Temperature Profile");
        graph_frame.add(&graph_area);
        vbox.add(&graph_frame);

        let mut graph_view = graph::View::new();
        graph_view.init(&graph_area);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let look_ahead = spin!(relm, hbox, "Lookahead time", 0.0, 60.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        hbox.add(&gtk::Label::new("Top element hysteresis:"));
        let top_hyst_on = spin!(relm, hbox, "on", -50.0, 0.0, 1.0);
        let top_hyst_off = spin!(relm, hbox, "off", -50.0, 50.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        hbox.add(&gtk::Label::new("Bottom element hysteresis:"));
        let bottom_hyst_on = spin!(relm, hbox, "on", -50.0, 0.0, 1.0);
        let bottom_hyst_off = spin!(relm, hbox, "off", -50.0, 50.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        let door_open_position = spin!(relm, hbox, "Door open position", 0.0, 16384.0, 1.0);

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        vbox.add(&hbox);
        hbox.add(&gtk::Label::new("Door hysteresis:"));
        let door_hyst_open = spin!(relm, hbox, "open", -50.0, 50.0, 1.0);
        let door_hyst_close = spin!(relm, hbox, "close", -50.0, 50.0, 1.0);

        let mut ret = Self {
            profiles,

            graph_area,
            graph_view,

            look_ahead,

            top_hyst_on,
            top_hyst_off,
            bottom_hyst_on,
            bottom_hyst_off,

            door_open_position,
            door_hyst_open,
            door_hyst_close,
        };

        ret.update_buttons(&model.persistent.bang_bang.active);
        (ret, vbox)
    }

    fn queue_redraw_graph(&self) {
        self.graph_area.queue_draw();
    }

    fn redraw_graph(&mut self, model: &mut BangBangData) {
        self.graph_view
            .draw(&model.graph_model, &mut self.graph_area);
        model.graph_model.did_redraw();
    }

    fn update_buttons(&mut self, settings: &BangBangSettings) {
        self.look_ahead.set_value(settings.look_ahead.into());

        self.top_hyst_on.set_value(settings.top_hyst_on.into());
        self.top_hyst_off.set_value(settings.top_hyst_off.into());
        self.bottom_hyst_on.set_value(settings.bottom_hyst_on.into());
        self.bottom_hyst_off.set_value(settings.bottom_hyst_off.into());

        self.door_open_position.set_value(settings.door_open_position.into());
        self.door_hyst_open.set_value(settings.door_hyst_open.into());
        self.door_hyst_close.set_value(settings.door_hyst_close.into());
    }

    fn update_settings(&self, settings: &mut BangBangSettings) {
        settings.look_ahead = self.look_ahead.get_value().into();

        settings.top_hyst_on = self.top_hyst_on.get_value().into();
        settings.top_hyst_off = self.top_hyst_off.get_value().into();
        settings.bottom_hyst_on = self.bottom_hyst_on.get_value().into();
        settings.bottom_hyst_off = self.bottom_hyst_off.get_value().into();

        settings.door_open_position = self.door_open_position.get_value() as _;
        settings.door_hyst_open = self.door_hyst_open.get_value().into();
        settings.door_hyst_close = self.door_hyst_close.get_value().into();
    }
}

impl OperationPanel for BangBangPanel {
    fn name(&self) -> &'static str {
        "Bang-Bang Control"
    }

    fn can_execute(&self) -> bool {
        true
    }

    fn get_operation(&self, model: &Model) -> Option<Operation> {
        //let pdata = &model.persistent.bang_bang;
        let persist = &model.persistent;
        let profile = persist.temp_profile.get_profile();
        Some(persist.bang_bang.active.to_control(&profile).into())
    }

    fn handle_event(&mut self, model: &mut Model, event: &Msg) {
        let pdata = &mut model.persistent.bang_bang;
        match event {
            Msg::ProfileDel => pdata.profiles.del_active(&mut self.profiles),

            Msg::ProfileLoad => {
                if let Some(profile) = pdata.profiles.get_active(&self.profiles) {
                    self.update_buttons(&profile);
                    pdata.active = profile;
                }
            }

            Msg::ProfileSave => {
                self.update_settings(&mut pdata.active);
                pdata.profiles.set_active(&mut self.profiles, &pdata.active);
            }

            Msg::StateUpdated => self.update_settings(&mut pdata.active),

            Msg::Redraw => self.redraw_graph(pdata),

            Msg::FocusChanged => {
                let profile = model.persistent.temp_profile.get_profile();
                *model.persistent.bang_bang.graph_model.get_profile_mut() = profile;
                self.queue_redraw_graph();
            },

            Msg::ControllerMessage(msg) => match msg {
                CtrlMsg::Running => {
                    info!("Clearing reports");
                    pdata.graph_model.clear_all_points();
                    self.queue_redraw_graph();
                }

                CtrlMsg::OperationReport(i, t, t2) => {
                    //info!("Adding new report: {:?}, {:?}", i, t);
                    pdata.graph_model.add_point(profile_graph::Series::Reported, *i, *t);
                    if let Some(temp) = t2 {
                        pdata.graph_model.add_point(profile_graph::Series::Secondary, *i, *temp);
                    }
                    self.queue_redraw_graph();
                }

                _ => {
                    //warn!("Ignoring message {:?}", msg);
                }
            },

            _ => (),
        }
    }
}
