#![allow(dead_code)]

use gtk::prelude::*;
use std::collections::BTreeMap;
use std::ops::Range;
use serde::{Deserialize, Serialize};

pub type Scale = Range<f64>;

#[derive(Clone, Debug)]
pub struct PathInfo {
    pub color: [f64; 4],
    pub width: f64,
    //pub point_radius: Option<f64>, // TODO
}

impl Default for PathInfo {
    fn default() -> Self {
        Self {
            color: [0.5, 0.5, 0.5, 1.0],
            width: 1.0,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Axis {
    pub scale: Scale,
    pub color: [f64; 4],   // R G B A; 0.0..1.0
    pub padding: [f64; 2], // negative, positive; pixels
    pub width: Option<f64>,
    pub label: Option<usize>, // Number of decimal places
}

impl Axis {
    fn total_padding(&self) -> f64 {
        self.padding.iter().sum()
    }
    fn required_size(&self) -> f64 {
        self.total_padding() as f64 + 1.0
    }
}

impl Default for Axis {
    fn default() -> Self {
        Self {
            scale: 0.0..0.0,
            color: [0.5, 0.5, 0.5, 1.0],
            padding: [20.0, 20.0],
            width: Some(1.0),
            label: Some(0),
        }
    }
}

// Absolute pixel position in widget; range 0.0.., right/down ascending.
#[derive(Debug, PartialEq, Copy, Clone)]
struct AbsolutePosition(pub f64, pub f64);

// Position relative to the axes; range 0.0..1.0, right/up ascending.
#[derive(Debug, PartialEq, Copy, Clone)]
struct RelativePosition(pub f64, pub f64);
impl RelativePosition {
    fn to_absolute(self, alloc: &gtk::Allocation, axes: &(Axis, Axis)) -> AbsolutePosition {
        /* Nudge positions to place lines on the extremes of the graph at the center of
         * each pixel.
         */
        let xsize = alloc.width as f64 - axes.0.total_padding() - 1.0;
        let ysize = alloc.height as f64 - axes.1.total_padding() - 1.0;

        if xsize < 0.0 || ysize < 0.0 {
            panic!() // View::draw() was supposed to give up.
        } else {
            AbsolutePosition(
                self.0 * xsize + axes.0.padding[0] + 0.5,
                self.1 * -ysize + alloc.height as f64 - axes.1.padding[0] + 0.5,
            )
        }
    }

    fn clamp(self) -> RelativePosition {
        RelativePosition(self.0.max(0.0).min(1.0), self.1.max(0.0).min(1.0))
    }
}

// Point position in native units
#[derive(Serialize, Deserialize, Debug, PartialEq, Copy, Clone)]
pub struct Point(pub f64, pub f64);
impl Point {
    fn to_relative(self, axes: &(Axis, Axis)) -> RelativePosition {
        let xscale = &axes.0.scale;
        let yscale = &axes.1.scale;
        RelativePosition(
            (self.0 - xscale.start) / (xscale.end - xscale.start),
            (self.1 - yscale.start) / (yscale.end - yscale.start),
        )
    }

    fn to_absolute(self, alloc: &gtk::Allocation, axes: &(Axis, Axis)) -> AbsolutePosition {
        self.to_relative(&axes).to_absolute(&alloc, &axes)
    }
}

pub trait Model {
    // XXX should needs_redraw clear automatically?
    fn needs_redraw(&self) -> bool;
    fn did_redraw(&mut self);

    fn axes(&self) -> (Axis, Axis);

    // FIXME: This is terrible, but I can't figure out how to return
    // Box<Iterator<Item=(PathInfo, Box<Iterator<Item=Point>>)>> from a trait.
    fn all_paths(&self) -> Vec<(PathInfo, Vec<Point>)>;
}

#[derive(Debug)]
pub struct VecModel<K: Ord> {
    changed: bool,
    series: BTreeMap<K, (PathInfo, Vec<Point>)>,
    axes: (Axis, Axis),
}

impl<K: Ord> VecModel<K> {
    pub fn new() -> VecModel<K> {
        let y_axis = Axis {
            scale: 0.0..0.0,
            color: [0.5, 0.5, 0.5, 1.0],
            padding: [20.0, 20.0],
            width: Some(1.0),
            label: Some(0),
        };

        let x_axis = Axis {
            padding: [20.0, 20.0],
            ..y_axis.clone()
        };

        VecModel {
            changed: true,
            series: BTreeMap::new(),
            axes: (x_axis, y_axis),
        }
    }

    fn get_series(&mut self, name: K) -> &mut (PathInfo, Vec<Point>) {
        self.changed = true;
        self.series.entry(name).or_insert_with(|| {
            (
                PathInfo {
                    color: [0.5, 0.5, 0.5, 1.0],
                    width: 1.0,
                    //point_radius: None,
                },
                Vec::new(),
            )
        })
    }

    pub fn axes_mut(&mut self) -> &mut (Axis, Axis) {
        self.changed = true;
        &mut self.axes
    }

    pub fn path_mut(&mut self, name: K) -> &mut PathInfo {
        &mut self.get_series(name).0
    }

    pub fn points_mut(&mut self, name: K) -> &mut Vec<Point> {
        &mut self.get_series(name).1
    }

    pub fn remove_series(&mut self, name: K) {
        self.changed = true;
        self.series.remove(&name);
    }
}

impl<K: Ord> Model for VecModel<K> {
    fn needs_redraw(&self) -> bool {
        self.changed
    }

    fn did_redraw(&mut self) {
        self.changed = false;
    }

    fn axes(&self) -> (Axis, Axis) {
        let mut min_x = self.axes.0.scale.start;
        let mut max_x = self.axes.0.scale.end;
        let mut min_y = self.axes.1.scale.start;
        let mut max_y = self.axes.1.scale.end;

        for (_, points) in self.series.values() {
            for &Point(x, y) in points {
                min_x = min_x.min(x);
                max_x = max_x.max(x);
                min_y = min_y.min(y);
                max_y = max_y.max(y);
            }
        }

        let mut ret = self.axes.clone();
        ret.0.scale = min_x..max_x;
        ret.1.scale = min_y..max_y;
        ret
    }

    fn all_paths(&self) -> Vec<(PathInfo, Vec<Point>)> {
        self.series.values().cloned().collect()
    }
}

// XXX can View own its DrawingArea?
pub struct View {
    handler: relm::DrawHandler<gtk::DrawingArea>,
    last_alloc: (i32, i32),
}

impl View {
    pub fn new() -> View {
        View {
            handler: relm::DrawHandler::new().expect("Create draw handler"),
            last_alloc: (-1, -1),
        }
    }

    pub fn init(&mut self, area: &gtk::DrawingArea) {
        self.handler.init(area);
    }

    fn was_resized(&mut self, alloc: &gtk::Allocation) -> bool {
        let new_alloc = (alloc.width, alloc.height);
        if self.last_alloc == new_alloc {
            false
        } else {
            self.last_alloc = new_alloc;
            true
        }
    }

    pub fn draw(&mut self, model: &dyn Model, area: &mut gtk::DrawingArea) {
        let alloc = area.get_allocation();
        if !model.needs_redraw() && !self.was_resized(&alloc) {
            return;
        }

        let ctx = self.handler.get_context();

        ctx.set_operator(cairo::Operator::Clear);
        ctx.paint();

        let axes = model.axes();
        if axes.0.required_size() > alloc.width as f64
            || axes.1.required_size() > alloc.height as f64
        {
            return;
        }

        ctx.set_operator(cairo::Operator::Over);

        // Draw the axes crossing the origin, or at least the closest visible point
        let origin = Point(0.0, 0.0)
            .to_relative(&axes)
            .clamp()
            .to_absolute(&alloc, &axes);

        let (ref xaxis, ref yaxis) = axes;
        if let Some(width) = xaxis.width {
            let c = &xaxis.color;
            ctx.set_source_rgba(c[0], c[1], c[2], c[3]);
            ctx.set_line_width(width);
            ctx.move_to(xaxis.padding[0], origin.1);
            ctx.line_to(alloc.width as f64 - xaxis.padding[1], origin.1);
            ctx.stroke();

            if let Some(places) = xaxis.label {
                let text = format!("{:.*}", places, xaxis.scale.start);
                let extents = ctx.text_extents(&text);
                ctx.move_to(
                    xaxis.padding[0] - extents.width / 2.0,
                    origin.1 + extents.height + 1.0,
                );
                ctx.show_text(&text);

                let text = format!("{:.*}", places, xaxis.scale.end);
                let extents = ctx.text_extents(&text);
                ctx.move_to(
                    alloc.width as f64 - xaxis.padding[1] - extents.width / 2.0,
                    origin.1 + extents.height + 1.0,
                );
                ctx.show_text(&text);
            }
        }
        if let Some(width) = yaxis.width {
            let c = &yaxis.color;
            ctx.set_source_rgba(c[0], c[1], c[2], c[3]);
            ctx.set_line_width(width);
            ctx.move_to(origin.0, yaxis.padding[1]);
            ctx.line_to(origin.0, alloc.height as f64 - yaxis.padding[0]);
            ctx.stroke();

            if let Some(places) = yaxis.label {
                let text = format!("{:.*}", places, yaxis.scale.start);
                let extents = ctx.text_extents(&text);
                ctx.move_to(
                    origin.0 - extents.width - 1.0,
                    alloc.height as f64 - yaxis.padding[0] + extents.height / 2.0,
                );
                ctx.show_text(&text);

                let text = format!("{:.*}", places, yaxis.scale.end);
                let extents = ctx.text_extents(&text);
                ctx.move_to(
                    origin.0 - extents.width - 1.0,
                    yaxis.padding[1] + extents.height / 2.0,
                );
                ctx.show_text(&text);
            }
        }

        for (info, points) in model.all_paths().iter() {
            let c = &info.color;
            ctx.set_source_rgba(c[0], c[1], c[2], c[3]);
            ctx.set_line_width(info.width);

            let mut points_iter = points.iter();
            if let Some(rel) = points_iter.next() {
                // TODO: also draw control points
                let AbsolutePosition(x, y) = rel.to_absolute(&alloc, &axes);
                ctx.move_to(x, y);
                for point in points_iter {
                    let AbsolutePosition(x, y) = point.to_absolute(&alloc, &axes);
                    ctx.line_to(x, y);
                }
                ctx.stroke();
            }
        }
    }
}
