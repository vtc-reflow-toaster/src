mod persistent;
mod state;

pub use persistent::Persistent;
pub use state::State;

pub type PersistentState = Persistent<State>;
