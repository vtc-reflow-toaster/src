use crate::ui::operation;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Default, Debug)]
#[serde(default)]
pub struct State {
    pub temp_profile: operation::temp_profile::TempProfileData,
    pub bang_bang: operation::bang_bang::BangBangData,
}
