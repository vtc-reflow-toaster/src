use serde::{de::DeserializeOwned, Serialize};
use std::fmt::Debug;
use std::fs::{self, File};
use std::io::prelude::*;
use std::ops::{Deref, DerefMut};
use std::path::PathBuf;

#[derive(Debug, Clone)]
pub struct Persistent<T: Default + Debug + Serialize + DeserializeOwned> {
    path: PathBuf,
    data: T,
}

impl<T: Default + Debug + Serialize + DeserializeOwned> Persistent<T> {
    pub fn new(basename: &str) -> Persistent<T> {
        let dirs = directories::ProjectDirs::from("edu", "VTC", "Reflow Toaster").unwrap();
        let mut buf = dirs.config_dir().to_path_buf();

        fs::create_dir_all(&buf).unwrap();

        buf.push(basename);
        buf.set_extension("json");

        match File::open(&buf).and_then(|mut f| {
            trace!("Reading configuration from {:?}", buf);
            let mut text = String::new();
            f.read_to_string(&mut text).unwrap();
            Ok(serde_json::from_str(&text).unwrap())
        }) {
            Ok(data) => Persistent { path: buf, data },
            Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => {
                info!("Creating default configuration");
                Persistent {
                    path: buf,
                    data: Default::default(),
                }
            }

            _ => panic!(),
        }
    }

    pub fn save_now(&self) {
        let json = serde_json::to_string(&self.data).unwrap();

        let mut tmpfile = self.path.clone();
        tmpfile.set_extension("tmp");
        let mut file = File::create(&tmpfile).unwrap();
        trace!("Writing state to {:?}", tmpfile);

        file.write_all(json.as_bytes()).unwrap();
        fs::rename(&tmpfile, &self.path).unwrap();
    }
}

impl<T: Default + Debug + Serialize + DeserializeOwned> Deref for Persistent<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.data
    }
}

impl<T: Default + Debug + Serialize + DeserializeOwned> DerefMut for Persistent<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.data
    }
}

// XXX this doesn't ever actually drop?!
impl<T: Default + Debug + Serialize + DeserializeOwned> Drop for Persistent<T> {
    fn drop(&mut self) {
        eprintln!("Saving persistent data on drop");
        self.save_now();
    }
}
